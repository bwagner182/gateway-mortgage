<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package gatewaymortgage
 */
?>

	<div class="row sticky-bottom padding-top-small padding-bottom-small sm-show md-hidden lg-hidden">
		<div class="container">
			<div class="col-sm-12">
			  	<div class="btn-wrapper">
			  		<a href="/request-a-quote/" title="request a quote" class="btn btn-green btn-large">Get Started</a>
			  	</div>
			</div>
		</div>
	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="container" id="footer-top">
			<div class="row flex-row">
				<div class="col-sm-12 col-md-6 col-lg-4 column1">
					<div class="image-wrapper">
						<a href="<?php echo get_bloginfo('home');?>">
							<img class="sm-hidden" src="<?php echo get_template_directory_uri();?>/static/img/logo__full-color.svg"/>
						</a>
					</div>
					<div class="sm-hidden md-hidden">
						<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-1')) :
                        endif; ?>
                    </div>
                    <div class="social-row">
                    	<a href="#" title="Like us on Facebook!"><span class="fa-stack"><i class="fal fa-circle fa-stack-2x"></i><i class="fab fa-facebook-f fa-stack-1x"></i></span></a>
                    	<a href="#" title="Follow us on Twitter"><span class="fa-stack"><i class="fal fa-circle fa-stack-2x"></i><i class="fab fa-twitter fa-stack-1x"></i></span></a>
                    	<a href="#" title="Follow us on LinkedIn"><span class="fa-stack"><i class="fal fa-circle fa-stack-2x"></i><i class="fab fa-linkedin fa-stack-1x"></i></span></a>
                    </div>
				</div>
				<div class="col-sm-12 col-md-6 col-lg-3">
					<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-2')) :
 
                    endif; ?>
				</div>
				<div class="col-sm-12 col-md-6 col-lg-3">
					<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-3')) :
 
                    endif; ?>
				</div>
				<div class="col-sm-12 col-md-6 col-lg-2">
					<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-4')) :
 
                    endif; ?>
				</div>

				<div class="col-sm-12 col-md-6 lg-hidden">
					<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-5')) :

                    endif; ?>
				</div>

			</div>
		</div>
		<div class="site-info">
			<div class="container" id="footer-bottom">
				<div class="row">
					<div class="col-md-10 col-lg-8">
						<p class="copyright">Gateway Mortgage is an equal opportunity St. Louis mortgage broker licensed to do business in the following states: NMLS #: 21149 | MO License #: 19-525 | FL License #: MBR995</p>
						<p class="copyright">&copy;<?php echo date('Y') . ' ' . bloginfo('sitename');?> | All Rights Reserved | Powered by <a href="drivesocialnow.com" title="Drive Social Media">Drive Social Media</a> | <a href="privacy-policy" title="privacy policy">Privacy Policy</a></p>
					</div>
				</div>
			</div>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>
</body>
</html>
