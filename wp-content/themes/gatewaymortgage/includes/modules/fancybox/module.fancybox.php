<?php

function fancybox_scripts() {
	wp_register_script( 'fancybox', 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js', 'jquery', '2.1.5', true );
	wp_register_style( 'fancybox', 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css', null, '2.1.5' );
	wp_enqueue_script( 'fancybox' );
	wp_enqueue_style( 'fancybox' );
}
add_action( 'wp_enqueue_scripts', 'fancybox_scripts' );
add_action( 'wp_footer', 'auto_fancy_box', 20 );

function auto_fancy_box() {
	?>
	<script>
		if( $.find('.gallery') ) {
			var gallery = $('.gallery');
			var id = gallery.attr('id');
			$( '.gallery-item a' ).each( function() {
				var href = $(this).attr('href');
				if (/\.(jpg|png|gif)$/.test(href)) {
					$(this).addClass('fancybox');
					$(this).attr('rel', id );
					$('.fancybox').fancybox();
				}
				
			});
		}

		$.each( $('[class*="wp-image"]'), function() {
			var parent = $(this).parent();
			var href = parent.attr("href");
			if (/\.(jpg|png|gif)$/.test(href)) {
				parent.addClass('fancybox');
				$('.fancybox').fancybox();
			} else {
				return false;
			}
		} );
	</script>
	<?php
}