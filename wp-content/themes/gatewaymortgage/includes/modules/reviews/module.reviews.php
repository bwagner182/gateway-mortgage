<?php

/*
** Create a module to have and store reviews
*/

if( function_exists('acf_add_options_sub_page') ) {

/*
 * First Create the Options Panel for the reivews
 */
acf_add_options_sub_page( array(
	'page_title' 	=> 'Review Settings',
	'menu_title' 	=> 'Review Settings',
	'parent_slug' 	=> 'edit.php?post_type=review'
));

/*
** Build Settings Page
 */

if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array (
	'key' => 'group_5728cab0cc21d',
	'title' => 'Reviews Options',
	'fields' => array (
		array (
			'key' => 'field_5728cab9bbba7',
			'label' => 'General',
			'name' => 'general',
			'type' => 'tab',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'placement' => 'top',
			'endpoint' => 0,
		),
		array (
			'key' => 'field_5728cacebbba8',
			'label' => 'Reviews have single',
			'name' => 'has_single',
			'type' => 'true_false',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'message' => 'Each Review has its own landing page',
			'default_value' => 0,
		),
		array (
			'key' => 'field_5728cd1049e2d',
			'label' => 'Reviews have Featured Image',
			'name' => 'reviews_have_featured_image',
			'type' => 'true_false',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'message' => 'Creates the option for the reiview to have an image attached',
			'default_value' => 0,
		),
		array (
			'key' => 'field_5728cf22e3ae7',
			'label' => 'Use Location',
			'name' => 'use_location',
			'type' => 'true_false',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'message' => 'Creates a State and City input on the Reviews Post Type',
			'default_value' => 0,
		),
		array (
			'key' => 'field_572a11f8c7a85',
			'label' => 'Assign Reviews to Post Type(s)',
			'name' => 'assign_reviews_to_post_types',
			'type' => 'checkbox',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => 100,
				'class' => '',
				'id' => '',
			),
			'choices' => array (
			),
			'default_value' => array (
			),
			'layout' => 'horizontal',
			'toggle' => 0,
		),
		array (
			'key' => 'field_5728d303687d8',
			'label' => 'Ratings',
			'name' => 'ratings',
			'type' => 'tab',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'placement' => 'top',
			'endpoint' => 0,
		),
		array (
			'key' => 'field_572a1faed37ea',
			'label' => 'Type of Rating',
			'name' => 'type_of_rating',
			'type' => 'radio',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => 100,
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				'simple' => 'Simple',
				'advanced' => 'Advanced',
			),
			'other_choice' => 0,
			'save_other_choice' => 0,
			'default_value' => 'simple',
			'layout' => 'horizontal',
		),
		array (
			'key' => 'field_572a1fe3bf0bb',
			'label' => 'Worst Possible Score',
			'name' => 'worst_possible_score',
			'type' => 'number',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_572a1faed37ea',
						'operator' => '==',
						'value' => 'simple',
					),
				),
			),
			'wrapper' => array (
				'width' => 20,
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'min' => '',
			'max' => '',
			'step' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_572a204bbf0bc',
			'label' => 'Best Possible Score',
			'name' => 'best_possible_score',
			'type' => 'number',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_572a1faed37ea',
						'operator' => '==',
						'value' => 'simple',
					),
				),
			),
			'wrapper' => array (
				'width' => 20,
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'min' => '',
			'max' => '',
			'step' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_572a20e7c6f9a',
			'label' => 'Advanced Scoring',
			'name' => 'advanced_scoring',
			'type' => 'repeater',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_572a1faed37ea',
						'operator' => '==',
						'value' => 'advanced',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'collapsed' => '',
			'min' => '',
			'max' => '',
			'layout' => 'table',
			'button_label' => 'Add Criteria',
			'sub_fields' => array (
				array (
					'key' => 'field_572a20f5c6f9b',
					'label' => 'Score Criteria',
					'name' => 'score_criteria',
					'type' => 'text',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => 70,
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
					'readonly' => 0,
					'disabled' => 0,
				),
				array (
					'key' => 'field_572a2108c6f9c',
					'label' => 'Best Score',
					'name' => 'best_score',
					'type' => 'number',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => 15,
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'min' => '',
					'max' => '',
					'step' => '',
					'readonly' => 0,
					'disabled' => 0,
				),
				array (
					'key' => 'field_572a2121c6f9d',
					'label' => 'Worst Score',
					'name' => 'worst_score',
					'type' => 'number',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => 15,
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'min' => '',
					'max' => '',
					'step' => '',
					'readonly' => 0,
					'disabled' => 0,
				),
			),
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'options_page',
				'operator' => '==',
				'value' => 'acf-options-review-settings',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));

endif;

// Set Up some Variables pulled from the options page

if( get_field( 'has_single', 'options' ) ) {
	$has_single = true;
	$permalink = 'permalink';
} else {
	$has_single = false;
	$permalink = null;
}
if( get_field( 'has_single', 'options' ) ) {
	$img = 'thumbanil';
} else {
	$img = null;
}


/// Create Review Post Type
function review_post_type() {
	$labels = array(
		'name'                => _x( 'Reviews', 'Post Type General Name', 'text_domain' ),
		'singular_name'       => _x( 'Review', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'           => __( 'Reviews', 'text_domain' ),
		'name_admin_bar'      => __( 'Review', 'text_domain' ),
		'parent_item_colon'   => __( 'Parent Item:', 'text_domain' ),
		'all_items'           => __( 'All Reviews', 'text_domain' ),
		'add_new_item'        => __( 'Add New', 'text_domain' ),
		'add_new'             => __( 'Add New', 'text_domain' ),
		'new_item'            => __( 'New Review', 'text_domain' ),
		'edit_item'           => __( 'Edit Review', 'text_domain' ),
		'update_item'         => __( 'Update Review', 'text_domain' ),
		'view_item'           => __( 'View Review', 'text_domain' ),
		'search_items'        => __( 'Search Reviews', 'text_domain' ),
		'not_found'           => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'text_domain' ),
	);
	$args = array(
		'label'               => __( 'Review', 'text_domain' ),
		'description'         => __( 'Reviews', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array( 'title', $permalink, $img ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'menu_icon'           => 'dashicons-format-quote',
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => false,
		'can_export'          => true,
		'has_archive'         => true,		
		'exclude_from_search' => false,
		'publicly_queryable'  => false,
		'rewrite'             => false,
		'capability_type'     => 'page',
	);
	register_post_type( 'review', $args );

}
add_action( 'init', 'review_post_type', 0 );

/*
** Create Fields for Reviews based on Settings
*/

//GEO Setting
if( get_field( 'use_location', 'options' ) ) { $geo = true; }

if( function_exists('acf_add_local_field_group') ):

if( $geo ) {
	acf_add_local_field_group(array (
		'key' => 'group_5728cdce8dc91',
		'title' => 'Review Details',
		'fields' => array (
			array (
				'key' => 'field_572a0d6428df1',
				'label' => 'Content',
				'name' => '',
				'type' => 'tab',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'placement' => 'top',
				'endpoint' => 0,
			),
			array (
				'key' => 'field_5728cde0904d4',
				'label' => 'Review Body',
				'name' => 'review_body',
				'type' => 'wysiwyg',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => 100,
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'tabs' => 'all',
				'toolbar' => 'full',
				'media_upload' => 0,
			),
			array (
				'key' => 'field_572a0d6f28df2',
				'label' => 'Location',
				'name' => '',
				'type' => 'tab',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'placement' => 'top',
				'endpoint' => 0,
			),
			array (
				'key' => 'field_572a0cf33b644',
				'label' => 'State',
				'name' => 'state',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => 20,
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
				'readonly' => 0,
				'disabled' => 0,
			),
			array (
				'key' => 'field_572a0d083b645',
				'label' => 'City',
				'name' => 'city',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => 20,
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
				'readonly' => 0,
				'disabled' => 0,
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'review',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => 1,
		'description' => '',
	));
} else {
	acf_add_local_field_group(array (
		'key' => 'group_5728cdce8dc91',
		'title' => 'Review Details',
		'fields' => array (
			array (
				'key' => 'field_5728cde0904d4',
				'label' => 'Review Body',
				'name' => 'review_body',
				'type' => 'wysiwyg',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => 100,
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'tabs' => 'all',
				'toolbar' => 'full',
				'media_upload' => 0,
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'review',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => 1,
		'description' => '',
	));
}

endif;

// Simple Vs Advanced 

$review_type = get_field( 'type_of_rating', 'options' );

if( $review_type == 'simple' ) {
	$best = get_field( 'best_possible_score', 'options' );
	$worst = get_field( 'worst_possible_score', 'options' );
	if( function_exists('acf_add_local_field_group') ):
		acf_add_local_field_group(array (
			'key' => 'group_572a22e911e62',
			'title' => 'Simple Rating',
			'fields' => array (
				array (
					'key' => 'field_572a22f72af04',
					'label' => 'Score',
					'name' => 'score',
					'type' => 'number',
					'instructions' => 'Range between '.$worst.' and '.$best,
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'min' => $worst,
					'max' => $best,
					'step' => '',
					'readonly' => 0,
					'disabled' => 0,
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'review',
					),
				),
			),
			'menu_order' => 0,
			'position' => 'side',
			'style' => 'default',
			'label_placement' => 'top',
			'instruction_placement' => 'label',
			'hide_on_screen' => '',
			'active' => 1,
			'description' => '',
		));
	endif;

} elseif( $review_type == 'advanced' ) {

	$fields = array();
	$citeria = get_field( 'advanced_scoring', 'options' );
	$i = 0;
	$string = 'number_';
	foreach( $citeria as $crit ) {
		$label = $crit['score_criteria'];
		$name = strtolower( str_replace(' ', '_', $label ) );
		$key = 'field_'.base64_encode($string.$i);
		$fields[] = array (
				'key' => $key,
				'label' => $label,
				'name' => $name,
				'type' => 'number',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => 20,
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'min' => $crit["worst_score"],
				'max' => $crit["best_score"],
				'step' => '',
				'readonly' => 0,
				'disabled' => 0,
			);
		$i++;
	}
	if( function_exists('acf_add_local_field_group') ):
	acf_add_local_field_group(array (
		'key' => 'group_572a421ad71b4',
		'title' => 'Advanced Rating',
		'fields' => $fields,
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'review',
				),
			),
		),
		'menu_order' => 1,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => 1,
		'description' => '',
	));
	endif;
}

/*
** Attach Reviews to specific Post Type(s)
*/

//Populate the Settings page with Current Post Types

function load_reviews_post_types( $field ) {
	$choices = array();
	$pts = get_post_types( array( 'public' => true, '_builtin' => false ), 'object' );
	unset($pts['review']);
	foreach ($pts as $pt) {
		$choices[$pt->name] = $pt->labels->name;
	}
	$field['choices'] = array();
	foreach($choices as $key => $value) {
		$field['choices'][$key] = $value;
	}
	return $field;
}
add_filter( 'acf/load_field/key=field_572a11f8c7a85', 'load_reviews_post_types' );


$cpts = get_field( 'assign_reviews_to_post_types', 'options' );

if( function_exists('acf_add_local_field_group') && $cpts ):
	acf_add_local_field_group(array (
		'key' => 'group_572a13e11b277',
		'title' => 'Review Attachment',
		'fields' => array (
			array (
				'key' => 'field_572a13fa29c8a',
				'label' => 'Attached to',
				'name' => 'attached_to',
				'type' => 'post_object',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'post_type' => $cpts,
				'taxonomy' => array (
				),
				'allow_null' => 0,
				'multiple' => 0,
				'return_format' => 'ID',
				'ui' => 1,
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'review',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'side',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => 1,
		'description' => '',
	));
endif;


/*
** Now that Our Reviews are set up,
** lets create some functions for
** pulling the data easily
*/

function get_reviews() {
	$args = array( 'post_type' => 'review' );
	$reviews = get_posts( $args );
	return $reviews;
}

function get_rating( $post = null ) {
	if( $post == null ) { global $post; }
	$id = $post->ID;
	$type = get_field( 'type_of_rating', 'options' );
	if( get_post_type( $post ) != 'review' ) {
		return null;
	} else {
		if( $type == 'simple' ) {
			return get_field( 'score', $id );
		} elseif( $type == 'advanced' ) {
			$rating = array();
			$citeria = get_field( 'advanced_scoring', 'options' );
			foreach( $citeria as $crit ) {
				$label = $crit['score_criteria'];
				$name = strtolower( str_replace(' ', '_', $label ) );
				$ratings[$name] = array( 'label' => $label, 'score' => get_field( $name, $id ) );
			}
			$overall_score = 0;
			foreach ($ratings as $rating) {
				$overall_score = $overall_score + $rating['score'];
			}
			$num_scores = count($ratings);
			$overall_score = round( $overall_score/$num_scores, 1 );
			$ratings['overall'] = array( 'label' => 'Overall', 'score' => $overall_score );
			return $ratings;
		} else {
			return null;
		}
	}

}


} // End ACF condition