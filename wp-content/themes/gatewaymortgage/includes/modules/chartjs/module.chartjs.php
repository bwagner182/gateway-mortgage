<?php

if( function_exists( 'get_field' ) ) {

require( 'module.chartjs_post-type.php' );
require( 'module.chartjs_meta-boxes.php' );

function chartjs_scripts() {
	wp_register_script( 'chartjs', 'https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.3.0/Chart.min.js', null, '2.3.0', true );
	wp_enqueue_script( 'chartjs' );
}
add_action( 'wp_enqueue_scripts', 'chartjs_scripts' );


function get_chartjs_json( $chart_id ) {
	$chart_type = get_field( 'chart_type', $chart_id );
	$chart_settings[ 'type' ] = $chart_type;

	// Build Chart Data based on type
	switch ( $chart_type ) {
		case 'bar':
			$data = get_chartjs_bar_chart_data( $chart_id );
			break;
		
		case 'line': 
			$data = get_chartjs_line_chart_data( $chart_id );
			break;
			
		default:
			$data = get_chartjs_pie_chart_data( $chart_id );
			break;
	}
	$chart_settings['data'] = $data;

	// encode teh setting to JSON format
	$chart_settings = json_encode( $chart_settings );

	return $chart_settings;
}


function get_chartjs_bar_chart_data( $chart_id ) {
	$labels = $bgc = $brdc = $data_value = array();

	// If there are dataSets present...
	if( have_rows( 'bar_data_sets', $chart_id ) ) {

		// ... add the values to arrays
		while( have_rows( 'bar_data_sets', $chart_id ) ) {
			the_row();
			$labels[] = get_sub_field( 'label' );
			$bgc[] = get_sub_field( 'background_color' );
			$brdc[] = get_sub_field( 'border_color' );
			$data_value[] = intval( get_sub_field( 'value' ) );
		}

		// .. then add the arrays to the chart object
		$data = array(
			'labels' => $labels,
			'datasets' => array(
				array(
					'label' => null,
					'backgroundColor' => $bgc,
					'borderColor' => $brdc,
					'borderWidth' => 1,
					'data' => $data_value
				)
			)
		);
		return $data;

	} else {
		return false;
	}
	
}

function get_chartjs_line_chart_data( $chart_id ) {

	// If there are dataSets present...
	if( have_rows( 'line_data_set', $chart_id ) ) {

		$labels = $data_sets = array();

		// Grab variables for the line
		while( have_rows( 'x_variables', $chart_id ) ) {
			the_row();
			$labels[] = get_sub_field( 'variable' );
		}

		// ... add the values to arrays
		while( have_rows( 'line_data_set', $chart_id ) ) {
			the_row();
			$values = array();

			// Grab values for the line
			while( have_rows( 'data_details' ) ) {
				the_row();
				$values[] = get_sub_field( 'value' );
			}

			$bgc = get_sub_field( 'line_color' );
			$bdrc = get_sub_field( 'line_border_color' );


			$data = array(
					'label' => get_sub_field( "label" ),
					'fill' => false,
					'lineTension' => 0.1,
					'backgroundColor' => $bgc,
					'borderColor' => $bdrc,
					'borderCapStyle' => 'butt',
		            'borderDash' => [],
		            'borderDashOffset' => 0.0,
		            'borderJoinStyle' => 'miter',
		            'pointBorderColor' => $bgc,
		            'pointBackgroundColor' => '#fff',
		            'pointBorderWidth' => 1,
		            'pointHoverRadius' => 5,
		            'pointHoverBackgroundColor' => $bgc,
		            'pointHoverBorderColor' => $bdrc,
		            'pointHoverBorderWidth' => 2,
		            'pointRadius' => 1,
		            'pointHitRadius' => 10,
		            'data' => $values,
		            'spanGaps' => false,
				);
			$data_sets[] = $data;
		}

		$data = array(
			'labels' => $labels,
			'datasets' => $data_sets
		);
		return $data;
	} else {
		return false;
	}		

}

function get_chartjs_pie_chart_data( $chart_id ) {

	if( have_rows( 'data_fields', $chart_id ) ) {
		$labels = $bgc = $brdc = $values = array();

		while( have_rows( 'data_fields', $chart_id ) ) {
			the_row();
			$labels[] = get_sub_field( 'data_field' );
			$bgc[] = get_sub_field( 'background_color' );
			if( get_sub_field( 'hover_color' ) ) {
				$brdc[] = get_sub_field( 'hover_color' );
			} else {
				$brdc[] = get_sub_field( 'background_color' );
			}
			$values[] = get_sub_field( 'value' );			
		}

		$data = array(
			'labels' => $labels,
			'datasets' => array(
				array(
					'data' => $values,
					'backgroundColor' => $bgc,
					'hoverBackgroundColor' => $brdc
				)
			),
		);
		return $data;
	} else {
		return false;
	}
	
}

} // END IF/ELSE CONDITION
