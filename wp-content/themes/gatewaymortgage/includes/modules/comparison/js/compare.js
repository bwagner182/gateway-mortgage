
var gatewaymortgageCookie = 'gatewaymortgage';
var $nonce = $( '#js-compare__nonce' );
$nonce = $nonce.attr( 'content' );

/*
** Events
*/

$( '.js-compare__add' ).on( 'click', function() {

	// If cookies are enabled
	if( cookieCheck() ) {

		var $this = $(this);
		var itemID = $this.data( 'id' );

		// Send AJAX Request
		$.ajax(
			{
				type: 'post',
				url: gatewaymortgageAjax.ajaxurl,
				data: {
					action: 'get_compare_data',
					id: itemID,
					nonce: $nonce
				},
				success: function( response ) {				
					var item = $.parseJSON( response );
					item = new Item( item.id, item.title, item.url );
					addItemCookie( item, gatewaymortgageCookie );
					
				},
				error: function( response ) {
					var message = $.parseJSON( response );
					alert( message );
				}
			}
		);
	}

});


$( '.js-compare__remove' ).on( 'click', function() {
	var $this = $(this);
	var itemID = $this.data( 'id' );

	// If cookies are enabled
	if( cookieCheck() ) {
		removeItemCookie( itemID, gatewaymortgageCookie );
		$this.remove();	
	}			

});


$(document).on( 'itemLimitExceeded', function() {
	alert( 'Too Many Items' );
} );

//$(document).on( 'itemRemoved', function() {
//	alert( 'Item was removed' );
//} );
//$(document).on( 'itemAdded', function() {
//	alert( 'Item was added' );
//} );


/*
** Functions
*/

// Build Item Constructor
function Item( id, title, url ) {
	this.id = id;
	this.title = title;
	this.url = url;
}

function cookieCheck() {
	var cookiesEnabled = navigator.cookieEnabled;
	var noCookiesError = 'You must enabled cookies to use comparison feature';
	if( !cookiesEnabled ) {
		alert( noCookiesError );
		return false;
	} else {
		return true;
	}
}

function setCookie(cname, cvalue, exdays) {
	var d = new Date();
	d.setTime(d.getTime() + (exdays*24*60*60*1000));
	var expires = "expires="+ d.toUTCString();
	//var expire = d.toUTCString();
	document.cookie = cname + "=" + cvalue + "; " + expires + ";path=/";
	//cookie( cname, cvalue, { expires: 1, path: '/' } )
}

function removeCookie(cname) {
	var d = new Date();
	var exdays = -1;
	d.setTime(d.getTime() + (exdays*24*60*60*1000));
	var expires = "expires="+ d.toUTCString();
	document.cookie = cname + "=; " + expires + ";path=/";
}

function getCookie(cname) {
	var name = cname + "=";
	var ca = document.cookie.split(';');
	for(var i = 0; i <ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') {
			c = c.substring(1);
		}
		if (c.indexOf(name) == 0) {
			return c.substring(name.length,c.length);
		}
	}
	return "";
}

function hasCookieItems( cname ) {
	var cookie = getCookie( cname );
	if( !cookie ) {
		return false;
	} else if( cookie.length > 0 ) {
		return true;
	} else {
		return false;
	}
}

function countCookieItems( cname ) {
	var cookie = getCookie( cname );
	if( cookie ) {
		cookie = JSON.parse( cookie );
		return cookie.length;
	} else {
		return false;
	}
	
}

function addItemCookie( item, cname ) {
	if( hasCookieItems( cname ) ) {
		var count = countCookieItems( cname );
		var cookie = getCookie( cname );

		if( count === 1 ) {
			var items = [];
			var firstItem = JSON.parse(cookie);
			firstItem = firstItem[0];

			// If the stored item is not equal to added item..
			if( firstItem != item.id ) {
				items.push( firstItem );
				items.push( item.id );
				items = JSON.stringify( items );
				setCookie( cname, items, 1 );
			} else {
				console.log( 'Same Item' );
			}	

			
		} else if( count > 1 ) {
			// Fire event when Item is added
			$.event.trigger({
				type: 'itemLimitExceeded',
				message: 'One or more items must be removed before adding more',
				date: new Date()
			});
		}

	} else {
		var items = [];
		items.push( item.id );
		items = JSON.stringify( items );
		console.log( items );
		setCookie( cname, items, 1 );
	}

	// Fire event when Item is added
	$.event.trigger({
		type: 'itemAdded',
		message: 'Item was added from Comparison Array',
		date: new Date()
	});
}


function removeItemCookie( value, cname ) {
	var cookie = getCookie( cname );
	cookie = $.parseJSON( cookie );
	var count = countCookieItems( cname );

	if( count === 0 ) {
		return false
	} else if( count < 2 ) {
		removeCookie( cname );
	} else {
		var removalIndex = $.inArray( value, cookie );

		if( removalIndex > 0 ) {
			cookie.splice( 1, removalIndex );
		} else {
			cookie.shift();
		}

		cookie = JSON.stringify( cookie );
		console.log( cookie );
		removeCookie( cname );
		setCookie( cname, cookie, 1 );	
	}

	// Fire event when Item is removed
	$.event.trigger({
		type: 'itemRemoved',
		message: 'Item was removed from Comparison Array',
		date: new Date()
	});

}
