<?php //Push to Close..

function compare_scripts()
{
    wp_register_script('gatewaymortgage-compare', get_template_directory_uri() . '/inc/modules/comparison/js/compare.js', 'jquery', null, true);
    wp_localize_script('gatewaymortgage-compare', 'gatewaymortgageAjax', array( 'ajaxurl' => admin_url('admin-ajax.php') ));

    wp_enqueue_script('gatewaymortgage-compare');
}
add_action('wp_enqueue_scripts', 'compare_scripts', 100);

function compare_nonce()
{
    $nonce = wp_create_nonce('gatewaymortgage');
    $nonce = '<meta id="js-compare__nonce" content="' . $nonce . '" />';
    echo $nonce;
}
add_action('wp_head', 'compare_nonce');


add_action('wp_ajax_nopriv_get_compare_data', 'get_compare_data');
add_action('wp_ajax_get_compare_data', 'get_compare_data');

function get_compare_data()
{
    if (!wp_verify_nonce($_POST['nonce'], "gatewaymortgage")) {
        exit("No naughty business please");
    }

    if (isset($_POST)) {
        $item_id = $_POST['id'];
        $result = get_post($item_id);
        //print_r($result);

        if ($result) {
            $item['id'] = $result->ID;
            $item['title'] = $result->post_title;
            $item['url'] = get_permalink($item_id);

            echo json_encode($item);
        } else {
            echo json_encode('No Posts Found');
        }
    } else {
        echo json_encode('Epic Fail');
    }
    die();
}

function get_compare_items($cname = null)
{
    if ($cname != null && isset($_COOKIE[$cname])) {
        $cookie = $_COOKIE[$cname];
        $cookie = json_decode($cookie);
        
        $items = get_posts(
            array(
                'include' => $cookie
            )
        );
        return $items;
    } else {
        return false;
    }
}

include('extend_module.comparison.php');
