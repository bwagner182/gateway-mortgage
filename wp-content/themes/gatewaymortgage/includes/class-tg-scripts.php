<?php

if (! defined('ABSPATH')) {
    exit;
}

class TG_Scripts
{

    /**
     * Contains an array of script handles registered.
     * @var array
     */
    private static $scripts = array();

    /**
     * Contains an array of script handles registered.
     * @var array
     */
    private static $styles = array();

    /**
     * Contains an array of script handles localized.
     * @var array
     */
    private static $wp_localize_scripts = array();

    /**
     * Hook in methods.
     */
    public static function init()
    {
        add_action('wp_enqueue_scripts', array( __CLASS__, 'load_scripts' ));
    }

    /**
     * Get styles for the frontend.
     * @access private
     * @return array
     */
    public static function get_styles()
    {
        return apply_filters('gatewaymortgage_enqueue_styles', array(
            'slick' => array(
                'src'   => str_replace(array('http:', 'https:'), '', get_template_directory_uri()) . '/static/css/slick.css',
                'deps'      => '',
                'version'   => null,
                'media'     => 'all',
            ),
            'fontawesome' => array(
                'src'       => str_replace(array('http:', 'https:'), '', get_template_directory_uri()) . '/static/css/all.min.css',
                'deps'      => '',
                'version'   => null,
                'media'     => 'all',
            ),
            'youtube-popup' => array(
                'src'       => str_replace(array('http:', 'https:'), '', get_template_directory_uri()) . '/static/css/YouTubePopUp.css',
                'deps'      => '',
                'version'   => null,
                'media'     => 'all',
            ),
            'swiper-css' => array(
                'src'       => str_replace(array('http:', 'https:'), '', get_template_directory_uri()) . '/static/css/swiper-bundle.css',
                'deps'      => '',
                'version'   => null,
                'media'     => 'all',
            ),
            'theme' => array(
                'src'     => str_replace(array( 'http:', 'https:' ), '', get_template_directory_uri()) . '/static/css/theme.css',
                'deps'    => '',
                'version' => null,
                'media'   => 'all'
            ),
        ));
    }

    /**
     * Register a script for use.
     *
     * @uses   wp_register_script()
     * @access private
     * @param  string   $handle
     * @param  string   $path
     * @param  string[] $deps
     * @param  string   $version
     * @param  boolean  $in_footer
     */
    private static function register_script($handle, $path, $deps = array( 'jquery' ), $version = '', $in_footer = true)
    {
        self::$scripts[] = $handle;
        wp_register_script($handle, $path, $deps, $version, $in_footer);
    }

    /**
     * Register and enqueue a script for use.
     *
     * @uses   wp_enqueue_script()
     * @access private
     * @param  string   $handle
     * @param  string   $path
     * @param  string[] $deps
     * @param  string   $version
     * @param  boolean  $in_footer
     */
    private static function enqueue_script($handle, $path = '', $deps = array( 'jquery' ), $version = '', $in_footer = true)
    {
        if (! in_array($handle, self::$scripts) && $path) {
            self::register_script($handle, $path, $deps, $version, $in_footer);
        }
        wp_enqueue_script($handle);
    }

    /**
     * Register a style for use.
     *
     * @uses   wp_register_style()
     * @access private
     * @param  string   $handle
     * @param  string   $path
     * @param  string[] $deps
     * @param  string   $version
     * @param  string   $media
     */
    private static function register_style($handle, $path, $deps = array(), $version = '', $media = 'all')
    {
        self::$styles[] = $handle;
        wp_register_style($handle, $path, $deps, $version, $media);
    }

    /**
     * Register and enqueue a styles for use.
     *
     * @uses   wp_enqueue_style()
     * @access private
     * @param  string   $handle
     * @param  string   $path
     * @param  string[] $deps
     * @param  string   $version
     * @param  string   $media
     */
    private static function enqueue_style($handle, $path = '', $deps = array(), $version = '', $media = 'all')
    {
        if (! in_array($handle, self::$styles) && $path) {
            self::register_style($handle, $path, $deps, $version, $media);
        }
        wp_enqueue_style($handle);
    }

    /**
     * Register/queue frontend scripts.
     */
    public static function load_scripts()
    {
        global $post;

        $assets_path 				= str_replace(array( 'http:', 'https:' ), '', get_template_directory_uri() .'/');
        $frontend_script_path 		= $assets_path . 'static/';
        //*
        // Register any scripts for later use, or used as dependencies
        self::register_script('jquery', $frontend_script_path . 'js/jquery-3.3.1.min.js', array(), null, false);
        self::register_script('bootstrap', $frontend_script_path . 'js/bootstrap.bundle.min.js', 'jquery', null, false);
        
        self::register_script('youtube-popup', $frontend_script_path . 'js/YouTubePopUp.jquery.js', 'jquery', null, true);
        self::register_script('site', $frontend_script_path . 'site-min.js', 'jquery', null, true);
        self::register_script('swiper', $frontend_script_path . 'js/swiper-bundle.js', 'jquery', null, true);
        
        // Register frontend scripts conditionally
        self::enqueue_script('jquery');
        self::enqueue_script('bootstrap');

        self::enqueue_script('site');
        
        self::enqueue_script('swiper');
        self::enqueue_script('youtube-popup');
        //*/
        // CSS Styles
        if ($enqueue_styles = self::get_styles()) {
            foreach ($enqueue_styles as $handle => $args) {
                self::enqueue_style($handle, $args['src'], $args['deps'], $args['version'], $args['media']);
            }
        }
    }
}
TG_Scripts::init();
