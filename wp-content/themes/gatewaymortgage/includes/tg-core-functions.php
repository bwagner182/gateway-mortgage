<?php

/*
 ** Clean Numbers for tel: links
 */
function clean_numbers($number, $echo = true)
{
    $no_spaces = preg_replace("/[^a-z0-9]/i", "", $number);
    $letters2 = preg_replace("/[a-c]/i", "2", $no_spaces);
    $letters3 = preg_replace("/[d-f]/i", '3', $letters2);
    $letters4 = preg_replace("/[g-i]/i", '4', $letters3);
    $letters5 = preg_replace("/[j-l]/i", '5', $letters4);
    $letters6 = preg_replace("/[m-o]/i", '6', $letters5);
    $letters7 = preg_replace("/[p-s]/i", '7', $letters6);
    $letters8 = preg_replace("/[t-v]/i", '8', $letters7);
    $letters9 = preg_replace("/[w-z]/i", '9', $letters8);
    if (!$echo) {
        return $letters9;
    } else {
        echo $letters9;
    }
}

/**
 * Remove version numbers from statc sources
 * @param array() $src
 */
function _remove_script_version($src)
{
    $parts = explode('?ver', $src);
    return $parts[0];
}
add_filter('script_loader_src', '_remove_script_version', 15, 1);
add_filter('style_loader_src', '_remove_script_version', 15, 1);
