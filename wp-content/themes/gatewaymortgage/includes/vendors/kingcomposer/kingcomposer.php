<?php
	add_action('init', 'ecopark_element_kingcomposer_map', 99 );
	add_action('init', 'ecopark_kc_add_parameters', 99 );

	/*
	 *
	 */
	function ecopark_kc_button_filter( $atts ){
		// die('d');

		// echo '<pre>'.print_r( $atts , 1);die;
		if( isset($atts['button_style']) || isset($atts['button_color']) || isset($atts['button_size']) ){
			if( !isset($atts['ex_class']) ){
				$atts['ex_class'] = '';
			}

			$button_style = isset($atts['button_style']) ? $atts['button_style'] : "";
			$button_color = isset($atts['button_color']) ? $atts['button_color'] : "";
			$button_size = isset($atts['button_size']) ? $atts['button_size'] : "";
			$atts['ex_class'] .= ' '.$button_style; 
			$atts['ex_class'] .= ' '.$button_color; 
			$atts['ex_class'] .= ' '.$button_size; 
		}
		return $atts; 
	}


 
	function ecopark_kc_add_parameters(){
	 
	    global $kc;
	    /// button
	    $kc->add_map_param( 
	        'kc_button', 
	        array(
	            'name' => 'button_style',
	               'label' => esc_html__('Button style', 'ecopark'),
	               'type' => 'select',
	               'admin_label' => false,
	               "options"     => array(
						'' => 'Default' , 
						'btn-outline'   => 'Outline',
						'btn-3d'   => '3D',
					),
	               'description' => esc_html__('Style particular content element differently - add a class name and refer to it in custom CSS.', 'ecopark')
	        ), 1 );

	     $kc->add_map_param( 
	        'kc_button', 
	        array(
	            'name' => 'button_color',
	               'label' => esc_html__('Button color', 'ecopark'),
	               'type' => 'select',
	               'admin_label' => false,
	               "options"     => array(
						'btn btn-default' => 'Default' , 
						'btn btn-primary'   => 'Green',
						'btn btn-danger'   => 'Red',
						'btn btn-info'   => 'Blue',
						'btn btn-warning'   => 'Yellow',
					),
	               'description' => esc_html__('Style particular content element differently - add a class name and refer to it in custom CSS.', 'ecopark')
	        ), 2 );

	     $kc->add_map_param( 
	        'kc_button', 
	        array(
	            'name' => 'button_size',
	               'label' => esc_html__('Button size', 'ecopark'),
	               'type' => 'select',
	               'admin_label' => false,
	               "options"     => array(
						'' => 'Default' , 
						'btn-xs'   => 'Small',
						'btn-sm'   => 'Medium',
						'btn-md'   => 'Large',
						'btn-lg'   => 'XLarge',
					),
	               'description' => esc_html__('Style particular content element differently - add a class name and refer to it in custom CSS.', 'ecopark')
	        ), 3 );
	}

	function ecopark_element_kingcomposer_map(){
		global $kc;

		$maps = array();
		$kc->add_filter( 'kc_button','ecopark_kc_button_filter' ,1  );
		// Degree-360
		$maps['element_block_heading'] =  array(

			"name"        => esc_html__("Block Heading", 'ecopark'),
			"class"       => "",
			"description" => esc_html__( 'Display Block Heading', 'ecopark' ),
			"category"    => esc_html__('Elements', 'ecopark'),
			"icon"        => 'kc-icon-title',
			"params"      => array(
				array(
					"type"        => "textfield",
					"label"       => esc_html__("Heading", 'ecopark'),
					"name"        => "heading",
					"value"       => '',
					"admin_label" => true
				),
				array(
					"type"        => "textfield",
					"label"       => esc_html__("Sub Heading", 'ecopark'),
					"name"        => "subheading",
					"value"       => '',
					"admin_label" => true
				),
				array(
					"type"        => "textarea",
					"label"       => esc_html__("Description", 'ecopark'),
					"name"        => "description",
					'description' => esc_html__( 'Display Description In heading.', 'ecopark' ),
					'admin_label' => true
				),
				array(
					"type"        => "select",
					"label"       => esc_html__("Style", 'ecopark'),
					"name"        => "style",
					"options"     => array(
						'default' => 'default' , 
						'v1'   => 'v1',
						'v2'   => 'v2',
						'v3'   => 'v3', 
					),
					'description' => esc_html__( 'Select style', 'ecopark' ),
					'admin_label' => true
				),
			)
		);

		// event list
		$maps['element_event_list'] =  array(
			"name"        => esc_html__("Event List", 'ecopark'),
			"class"       => "",
			"description" => 'A widget that displays upcoming events.',
			"category"    => esc_html__('Elements', 'ecopark'),
			"icon"        => 'cpicon sl-paper-plane',
			'is_container' => true,
			"params"      => array(
				array(
					"type"        => "textfield",
					"label"       => esc_html__("Title", 'ecopark'),
					"name"        => "title",
					"value"       => 'Upcoming Events',
					"admin_label" => true
				),
				array(
					"type"        => "textfield",
					"label"       => esc_html__("Show", 'ecopark'),
					"name"        => "show",
					"value"       => '5',
					"admin_label" => true
				),
			)
		);

		// event carousel
		$maps['element_event_carousel'] =  array(
			"name"        => esc_html__("Event Carousel", 'ecopark'),
			"class"       => "",
			"description" => 'A widget that displays upcoming events.',
			"category"    => esc_html__('Elements', 'ecopark'),
			"icon"        => 'cpicon sl-paper-plane',
			'is_container' => true,
			"params"      => array(
				array(
					"type"        => "textfield",
					"label"       => esc_html__("Title", 'ecopark'),
					"name"        => "title",
					"value"       => 'Events',
					"admin_label" => true
				),
				array(
					"type"        => "textfield",
					"label"       => esc_html__("Sub Title", 'ecopark'),
					"name"        => "sub_title",
					"value"       => "What's Happening",
					"admin_label" => true
				),
				array(
					"type"        => "textfield",
					"label"       => esc_html__("Show", 'ecopark'),
					"name"        => "show",
					"value"       => '5',
					"admin_label" => true
				),
				array(
					'type' => 'number_slider',
					'label' => esc_html__( 'Grid Column' ,'ecopark' ),
					'name' => 'columns',
					'options' => array(
						'min' => 1,
						'max' => 6,
						'unit' => '',
						'show_input' => true
					),
					"admin_label" => true,
					'description' => 'Display number of post'
				),
			)
		);

		// room grid
		$maps['element_grid_accommodation'] =  array(
			"name"         => esc_html__("Grid Accommodations",'ecopark'),
			"class"        => "",
			"description"  =>'Display Accommodations on frontend',
			"category"     => esc_html__('Elements', 'ecopark'),
			"icon"         => 'cpicon sl-paper-plane',
			'is_container' => true,
			"params"       => array(
				array(
					'type' => 'number_slider',
					'label' => esc_html__( 'Grid Column' ,'ecopark' ),
					'name' => 'columns',
					'options' => array(
						'min' => 1,
						'max' => 6,
						'unit' => '',
						'show_input' => true
					),
					"admin_label" => true,
					'description' => 'Display number of column'
				),
				array(
					"type"        => "select",
					"label"       => esc_html__("Order By", 'ecopark'),
					"name"        => "orderby",
					"options"     => array( 
						'most_recent'   => 'Lastest Accommodations',
						'random'   => 'Random Accommodations', 
					),
					'description' => esc_html__( 'Select order by', 'ecopark' ),
					'admin_label' => true
				),
				array(
					"type"        => "select",
					"label"       => esc_html__("Style", 'ecopark'),
					"name"        => "layout",
					"options"     => array(
						'default' => 'default' , 
						'overlap'   => 'overlap', 
					),
					'description' => esc_html__( 'Select style', 'ecopark' ),
					'admin_label' => true
				),
				array(
					"type"        => "textfield",
					"label"       => esc_html__("Number", 'ecopark'),
					"name"        => "number",
					"value"       => '8',
					"admin_label" => true
				),
				array(
					"type"        => "select",
					"label"       => esc_html__("Enable Pagination", 'ecopark'),
					"name"        => "pagination",
					"options"     => array(
						'0' => 'No' , 
						'1'   => 'Yes',
					),
					'description' => esc_html__( 'Show paginaion', 'ecopark' ),
					'admin_label' => true
				),
			)
		);

		// room carousel
		$maps['element_carousel_accommodation'] =  array(
			"name"         => esc_html__("Carousel Accommodations",'ecopark'),
			"class"        => "",
			"description"  =>'Display Accommodations on frontend',
			"category"     => esc_html__('Elements', 'ecopark'),
			"icon"         => 'cpicon sl-paper-plane',
			'is_container' => true,
			"params"       => array(
				array(
					'type' => 'number_slider',
					'label' => esc_html__( 'Grid Column' ,'ecopark' ),
					'name' => 'columns',
					'options' => array(
						'min' => 1,
						'max' => 6,
						'unit' => '',
						'show_input' => true
					),
					"admin_label" => true,
					'description' => 'Display number of column'
				),
				array(
					"type"        => "select",
					"label"       => esc_html__("Style", 'ecopark'),
					"name"        => "layout",
					"options"     => array(
						'default' => 'default' , 
						'overlap'   => 'overlap', 
					),
					'description' => esc_html__( 'Select style', 'ecopark' ),
					'admin_label' => true
				),
				array(
					"type"        => "select",
					"label"       => esc_html__("Order By", 'ecopark'),
					"name"        => "orderby",
					"options"     => array( 
						'most_recent'   => 'Lastest Accommodations',
						'random'   => 'Random Accommodations', 
					),
					'description' => esc_html__( 'Select order by', 'ecopark' ),
					'admin_label' => true
				),
				array(
					"type"        => "textfield",
					"label"       => esc_html__("Number", 'ecopark'),
					"name"        => "number",
					"value"       => '8',
					"admin_label" => true
				),
			)
		);

		// reservation form
		$maps['element_reservation_form'] =  array(
			"name"         => esc_html__("Reservation Form",'ecopark'),
			"class"        => "",
			"description"  =>'Display Reservation Form on frontend',
			"category"     => esc_html__('Elements', 'ecopark'),
			"icon"         => 'cpicon sl-paper-plane',
			'is_container' => true,
			"params"       => array(
				array(
					"type"        => "select",
					"label"       => esc_html__("Layout", 'ecopark'),
					"name"        => "layout",
					"options"     => array(
						'' => 'Vertical Form' , 
						'horizontal'   => 'Horizontal Form',
					),
					'description' => esc_html__( 'Select form style', 'ecopark' ),
					'admin_label' => true
				),
			)
		);

		// custom box
		$maps['element_custom_box'] = array(
			"name"		   => esc_html__("Custom Box", 'ecopark'),
			"class"        => "",
			"description"  =>'Display Reservation Form on frontend',
			"category"     => esc_html__('Elements', 'ecopark'),
			"icon"         => 'cpicon sl-paper-plane',
			"params"       => array(
				array(
					"type"        => "textfield",
					"label"       => esc_html__("Heading", 'ecopark'),
					"name"        => "heading",
					"value"       => '',
					"admin_label" => true
				),
				array(
					"type"        => "textarea",
					"label"       => esc_html__("Description", 'ecopark'),
					"name"        => "description",
					'description' => esc_html__( 'Display Description In heading.', 'ecopark' ),
					'admin_label' => true
				),
				array(
					'type'  => 'attach_image',
					'label' => esc_html__( 'Image', 'ecopark' ),
					'name'  => 'image',
				),
				array(
					'type'  => 'attach_image',
					'label' => esc_html__( 'Logo', 'ecopark' ),
					'name'  => 'logo',
				),
				array(
					"type"        => "textfield",
					"label"       => esc_html__("Text Link", 'ecopark'),
					"name"        => "text_link",
					"value"       => '',
					"admin_label" => true
				),
				array(
					"type"        => "textfield",
					"label"       => esc_html__("Link", 'ecopark'),
					"name"        => "link",
					"value"       => '',
					"admin_label" => true
				),
			)
		);

		$maps = apply_filters( 'ecopark_element_kingcomposer_map', $maps );
		$kc->add_map( $maps );
	} // end class
?>