<?php  
add_action( 'customize_register',  'ecopark_register_activity_customizer',99);
function ecopark_register_activity_customizer( $wp_customize ){
    $wp_customize->add_panel( 'panel_opalactivity', array(
        'priority' => 70,
        'capability' => 'edit_theme_options',
        'theme_supports' => '',
        'title' => esc_html__( 'Opal Activitys', 'ecopark' ),
        'description' =>esc_html__( 'Make default setting for page, general', 'ecopark' ),
    ) );
    //============================================================================
    // Sidebar for Archive Activity 
    //============================================================================    
    

    /**
     * Archive Setting
     */
    $wp_customize->add_section( 'archive_activity_settings', array(
        'priority' => 11,
        'capability' => 'edit_theme_options',
        'theme_supports' => '',
        'title' => esc_html__( 'Archive & Categgory Setting', 'ecopark' ),
        'description' => '',
        'panel' => 'panel_opalactivity',
    ) );


    //sidebar archive left
    $wp_customize->add_setting( 'wpopal_theme_options[activity-achive-page-left-sidebar]', array(
        'capability' => 'edit_theme_options',
        'type'       => 'option',
        'default'   => '',
        'checked' => 1,
        'sanitize_callback' => 'sanitize_text_field'
    ) );

    $wp_customize->add_control( new Wpopal_Themer_Sidebar_DropDown( $wp_customize,  'wpopal_theme_options[activity-achive-page-left-sidebar]', array(
        'settings'  => 'wpopal_theme_options[activity-achive-page-left-sidebar]',
        'label'     => esc_html__('Left Sidebar', 'ecopark'),
        'section'   => 'archive_activity_settings' ,
         'priority' => 3
    ) ) );

      //sidebar archive right
    $wp_customize->add_setting( 'wpopal_theme_options[activity-achive-page-right-sidebar]', array(
        'capability' => 'edit_theme_options',
        'type'       => 'option',
        'default'   => '',
        'checked' => 1,
        'sanitize_callback' => 'sanitize_text_field'
    ) );

    $wp_customize->add_control( new Wpopal_Themer_Sidebar_DropDown( $wp_customize,  'wpopal_theme_options[activity-achive-page-right-sidebar]', array(
        'settings'  => 'wpopal_theme_options[activity-achive-page-right-sidebar]',
        'label'     => esc_html__('Right Sidebar', 'ecopark'),
        'section'   => 'archive_activity_settings',
         'priority' => 4 
    ) ) );



    //============================================================================
        // Sidebar for Single Activity 
    //============================================================================

    /**
     * Single Page Setting
     */
    $wp_customize->add_section( 'activity_single_settings', array(
        'priority' => 11,
        'capability' => 'edit_theme_options',
        'theme_supports' => '',
        'title' => esc_html__( 'Single Activity Page Setting', 'ecopark' ),
        'description' => 'Configure Activity page setting',
        'panel' => 'panel_opalactivity',
    ) );



    //sidebar single left
    $wp_customize->add_setting( 'wpopal_theme_options[activity-page-left-sidebar]', array(
        'capability' => 'edit_theme_options',
        'type'       => 'option',
        'default'   => '',
        'checked' => 1,
        'sanitize_callback' => 'sanitize_text_field'
    ) );

    $wp_customize->add_control( new Wpopal_Themer_Sidebar_DropDown( $wp_customize,  'wpopal_theme_options[activity-page-left-sidebar]', array(
        'settings'  => 'wpopal_theme_options[activity-page-left-sidebar]',
        'label'     => esc_html__('Left Sidebar', 'ecopark'),
        'section'   => 'activity_single_settings' ,
         'priority' => 3
    ) ) );

      //sidebar single right
    $wp_customize->add_setting( 'wpopal_theme_options[activity-page-right-sidebar]', array(
        'capability' => 'edit_theme_options',
        'type'       => 'option',
        'default'   => '',
        'checked' => 1,
        'sanitize_callback' => 'sanitize_text_field'
    ) );

    $wp_customize->add_control( new Wpopal_Themer_Sidebar_DropDown( $wp_customize,  'wpopal_theme_options[activity-page-right-sidebar]', array(
        'settings'  => 'wpopal_theme_options[activity-page-right-sidebar]',
        'label'     => esc_html__('Right Sidebar', 'ecopark'),
        'section'   => 'activity_single_settings',
         'priority' => 4 
    ) ) );

}

/**
* Get Configuration for Page Layout
*
*/
function ecopark_fnc_get_single_page_activity_sidebar_configs( $configs='' ){

global $post; 

$left  =  ecopark_fnc_theme_options( 'activity-page-left-sidebar' ); 
$right =  ecopark_fnc_theme_options( 'activity-page-right-sidebar' ); 

return ecopark_fnc_get_layout_configs( $left, $right);
}
add_filter( 'ecopark_fnc_get_single_activity_sidebar_configs', 'ecopark_fnc_get_single_page_activity_sidebar_configs', 1, 1 );
/**
* Get Configuration for Page Layout
*
*/
function ecopark_fnc_get_achive_page_activity_sidebar_configs( $configs='' ){

global $post; 

$left  =  ecopark_fnc_theme_options( 'activity-achive-page-left-sidebar' ); 
$right =  ecopark_fnc_theme_options( 'activity-achive-page-right-sidebar' ); 

return ecopark_fnc_get_layout_configs( $left, $right);
}
add_filter( 'ecopark_fnc_get_archive_activity_sidebar_configs', 'ecopark_fnc_get_achive_page_activity_sidebar_configs', 1, 1 );


