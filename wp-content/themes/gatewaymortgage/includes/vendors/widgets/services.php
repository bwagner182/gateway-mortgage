<?php
/**
 * $Desc
 *
 * @version    $Id$
 * @package    wpbase
 * @author      Team <opalwordpressl@gmail.com >
 * @copyright  Copyright (C) 2015  prestabrain.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * @website  http://www.wpopal.com
 * @support  http://www.wpopal.com/questions/
 */
class Ecopark_Services_Widget extends WP_Widget {
    public function __construct() {
        parent::__construct(
            // Base ID of your widget
            'ecopark_services_widget',
            // Widget name will appear in UI
            esc_html__('(Ecopark) Services', 'ecopark'),
            // Widget description
            array( 'description' => esc_html__( 'Services', 'ecopark' ), )
        );
    }

    /**
     * The main widget output function.
     * @param array $args
     * @param array $instance
     * @return string The widget output (html).
     */
    public function widget( $args, $instance ) {
        extract( $args, EXTR_SKIP );
        extract( $instance, EXTR_SKIP );

        $title = apply_filters( 'widget_title', $instance['title'] );
       
        echo trim($before_widget);
        ?>
        <?php switch ($instance['layout']) { 
            case 'header_right':?>
                <div class="support-style-1">
                <?php if(!empty($instance['supportcall'])):?>
                <div class="textwidget">
                    <i class="fa fa-phone" aria-hidden="true"></i>
                    <div class="pull-left">
                    <span><?php esc_html_e('support call','ecopark');?></span>
                    <a><?php echo esc_html($instance['supportcall']); ?></a>
                    </div>
                </div>
                <?php endif; ?>
                <?php if(!empty($instance['location'])):?>
                <div class="textwidget">
                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                    <div class="pull-left">
                    <span><?php esc_html_e('location','ecopark');?></span>
                    <a><?php echo esc_html($instance['location']);?></a>
                    </div>
                </div>
                <?php endif; ?>
                </div>
            <?php break; ?>
        <?php case 'header_left': ?>
                <div class="support-style-2">
                <?php if(!empty($instance['supportcall'])):?>
                    <i class="fa fa-phone"></i><a class="text-primary" href="#"><?php echo esc_html($instance['supportcall']); ?></a>
                <?php endif; ?>
                <?php if(!empty($instance['location'])):?>
                    <i class="fa fa-map-marker"></i><span class="text-primary"><?php echo esc_html($instance['location']);?></span>
                <?php endif; ?>
                </div>
                <?php break; ?>
        <?php default: ?>
                <div class="support-style-3">
                <h3 class="widget-title"><?php echo esc_html($instance['title']); ?></h3>

                <?php if(!empty($instance['address'])):?>
                    <p><?php echo esc_html($instance['address']); ?></p>
                <?php endif; ?>
                
                <ul class="list-inline">
                <?php if(!empty($instance['reservations'])):?>
                    <li>
                        <span><?php esc_html_e( 'Reservations', 'ecopark' ); ?></span>
                        <strong><?php echo esc_html($instance['reservations']); ?></strong>
                        <?php if(!empty($instance['emailtxt'])):?>
                            <?php if(empty($instance['emaillink']) || !isset($instance['emaillink'])):?>
                                <a class="text-primary" href="#"><?php echo esc_html($instance['emailtxt']); ?></a>
                            <?php else: ?>
                                <a class="text-primary" href="<?php echo trim(@$instance['emaillink']); ?>"><?php echo esc_html($instance['emailtxt']); ?></a>
                            <?php endif; ?>
                        <?php endif; ?>
                    </li>
                <?php endif; ?>
                <?php if(!empty($instance['administration'])):?>
                <li>
                    <span><?php esc_html_e( 'Administration', 'ecopark' ); ?></span>
                    <strong><?php echo esc_html($instance['administration']); ?></strong>
                </li>
                <?php endif; ?>
                </ul>
                </div> 
            <?php break; ?>
        <?php } //end switch ?>
           
        <?php
        echo trim($after_widget);
    }

    /**
     * The function for saving widget updates in the admin section.
     * @param array $new_instance
     * @param array $old_instance
     * @return array The new widget settings.
     */
     
    public function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $new_instance = $this->default_instance_args( $new_instance );

        /* Strip tags (if needed) and update the widget settings. */
        $instance['title']   = $new_instance['title'];
        $instance['supportcall']   = $new_instance['supportcall'];
        $instance['location'] = $new_instance['location'];
        $instance['layout'] = $new_instance['layout'];
        $instance['address'] = $new_instance['address'];
        $instance['reservations'] = $new_instance['reservations'];
        $instance['emailtxt'] = $new_instance['emailtxt'];
        $instance['emaillink'] = $new_instance['emaillink'];
        $instance['administration'] = $new_instance['administration'];


        return $instance;
    }

    /**
     * Output the admin form for the widget.
     * @param array $instance
     * @return string The output for the admin widget form.
     */
    public function form( $instance ) {
        $instance  = $this->default_instance_args( $instance );
        $selectedR = "";
        $selectedL = "";
        $selectedF = "";
        if( $instance['layout'] == 'header_right'){
            $selectedR = "selected='selected'";
        }elseif( $instance['layout'] == 'header_left'){
            $selectedL = "selected='selected'";
        }else{
            $selectedF = "selected='selected'";
        }

    ?>
    <div class="wpopal_recentpost">
        
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'Layout' ) ); ?>"><?php esc_html_e( 'Layout:', 'ecopark' ); ?></label>
            <select class="widefat service-layout" id="<?php echo esc_attr( $this->get_field_id( 'layout' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'layout' ) ); ?>">
                <option value="header_right" <?php echo esc_attr($selectedR) ;?>><?php esc_html_e('Header Right', 'ecopark'); ?></option>
                <option value="header_left"  <?php echo esc_attr($selectedL) ;?>><?php esc_html_e('Header Left', 'ecopark'); ?> </option>
                <option value="footer"  <?php echo esc_attr($selectedF) ;?>><?php esc_html_e('Footer', 'ecopark'); ?></option>
            </select>
        </p>
        <div class="service-header">
        <p>
           <h4><?php esc_html_e('Config Service Header ','ecopark');?></h4><hr>
        </p>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'supportcall' ) ); ?>"><?php esc_html_e( 'Support Call', 'ecopark' ); ?></label>
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'supportcall' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'supportcall' ) ); ?>" type="text" value="<?php echo esc_attr( $instance['supportcall'] ); ?>" />
        </p>
        
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'location' ) ); ?>"><?php esc_html_e( 'Location:', 'ecopark' ); ?></label>
            <textarea rows="8" class="widefat" name="<?php echo esc_attr( $this->get_field_name( 'location' ) ); ?>" id="<?php echo esc_attr( $this->get_field_id( 'location' ) ); ?>"><?php echo esc_attr( $instance['location'] ); ?></textarea>

        </p>
        </div>
        <div class="service-footer">
            <p>
               <hr>
               <h4><?php esc_html_e('Config Service Footer','ecopark');?> </h4>
            </p>

            <p>
                <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title', 'ecopark' ); ?></label>
                <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $instance['title'] ); ?>" />
            </p>

            <p>
                <label for="<?php echo esc_attr( $this->get_field_id( 'address' ) ); ?>"><?php esc_html_e( 'Address:', 'ecopark' ); ?></label>
                <textarea rows="8" class="widefat" name="<?php echo esc_attr( $this->get_field_name( 'address' ) ); ?>" id="<?php echo esc_attr( $this->get_field_id( 'address' ) ); ?>"><?php echo esc_attr( $instance['address'] ); ?></textarea>

            </p>

            <p>
                <label for="<?php echo esc_attr( $this->get_field_id( 'reservations' ) ); ?>"><?php esc_html_e( 'Reservations', 'ecopark' ); ?></label>
                <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'reservations' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'reservations' ) ); ?>" type="text" value="<?php echo esc_attr( $instance['reservations'] ); ?>" />
            </p>

            <p>
                <label for="<?php echo esc_attr( $this->get_field_id( 'emailtxt' ) ); ?>"><?php esc_html_e( 'Sub Text Reservations ', 'ecopark' ); ?></label>
                <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'emailtxt' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'emailtxt' ) ); ?>" type="text" value="<?php echo esc_attr( $instance['emailtxt'] ); ?>" />
                <span>Default: EMAIL US - MAP</span>
            </p>

            <p>
                <label for="<?php echo esc_attr( $this->get_field_id( 'emaillink' ) ); ?>"><?php esc_html_e( 'Link Sub Text Reservations ', 'ecopark' ); ?></label>
                <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'emaillink' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'emaillink' ) ); ?>" type="text" value="<?php echo esc_attr( $instance['emaillink'] ); ?>" />
                <span>Default: mailto:name@email.com</span>
            </p>

            <p>
                <label for="<?php echo esc_attr( $this->get_field_id( 'administration' ) ); ?>"><?php esc_html_e( 'Administration', 'ecopark' ); ?></label>
                <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'administration' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'administration' ) ); ?>" type="text" value="<?php echo esc_attr( $instance['administration'] ); ?>" />
            </p>
        </div>
        
    </div>
<?php
    }

    /**
     * Accepts and returns the widget's instance array - ensuring any missing
     * elements are generated and set to their default value.
     * @param array $instance
     * @return array
     */
    protected function default_instance_args( array $instance ) {
        return wp_parse_args( $instance, array(
            'title'   => esc_html__( 'Ecopark', 'ecopark' ),
            'layout'   => esc_html__( 'Services', 'ecopark' ),
            'supportcall'  => '',
            'location'  => '',
            'address'  => '',
            'reservations'  => '',
            'emailtxt'  => esc_html__( 'EMAIL US - MAP', 'ecopark' ),
            'emaillink'  => 'mailto:name@email.com',
            'administration'  => '',
        ) );
    }
}

register_widget( 'Ecopark_Services_Widget' );
