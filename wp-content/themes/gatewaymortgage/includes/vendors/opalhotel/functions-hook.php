<?php  
 
function ecopark_opalhotel_template_path(){
    return '/opalhotel/';
}
add_filter( 'opalhotel_template_path', 'ecopark_opalhotel_template_path' );

function ecopark_fnc_get_single_room_sidebar_configs( $configs='' ){

    global $post; 

    $left   =  ecopark_fnc_theme_options( 'opalhotel-single-left-sidebar', 'opalhotel-sidebar-left');
    $right  =  ecopark_fnc_theme_options( 'opalhotel-single-right-sidebar', 'opalhotel-sidebar-right');

 
    
    return ecopark_fnc_get_layout_configs($left, $right);
}
add_filter( 'ecopark_fnc_get_single_room_sidebar_configs', 'ecopark_fnc_get_single_room_sidebar_configs', 1, 1 );

function ecopark_fnc_get_archive_room_sidebar_configs( $configs='' ){

    global $post;
    
    $left  =  ecopark_fnc_theme_options( 'opalhotel-archive-left-sidebar' ); 

    $right =  ecopark_fnc_theme_options( 'opalhotel-archive-right-sidebar' );

    return ecopark_fnc_get_layout_configs($left, $right);
}

add_filter( 'ecopark_fnc_get_archive_room_sidebar_configs', 'ecopark_fnc_get_archive_room_sidebar_configs', 1, 1 );

/* Hotel */
function ecopark_fnc_get_single_hotel_sidebar_configs( $configs='' ){

    global $post; 

    $left   =  ecopark_fnc_theme_options( 'opalhotel-single-hotel-left-sidebar', 'opalhotel-sidebar-left');
    $right  =  ecopark_fnc_theme_options( 'opalhotel-single-hotel-right-sidebar', 'opalhotel-sidebar-right');

 
    
    return ecopark_fnc_get_layout_configs($left, $right);
}
add_filter( 'ecopark_fnc_get_single_hotel_sidebar_configs', 'ecopark_fnc_get_single_hotel_sidebar_configs', 1, 1 );

function ecopark_fnc_get_archive_hotel_sidebar_configs( $configs='' ){

    global $post;
    
    $left  =  ecopark_fnc_theme_options( 'opalhotel-archive-hotel-left-sidebar' ); 

    $right =  ecopark_fnc_theme_options( 'opalhotel-archive-hotel-right-sidebar' );

    return ecopark_fnc_get_layout_configs($left, $right);
}

add_filter( 'ecopark_fnc_get_archive_hotel_sidebar_configs', 'ecopark_fnc_get_archive_hotel_sidebar_configs', 1, 1 );

/**
 *
 *
 */
function ecopark_opalhotel_room_query( $type='', $number=4, $paged=1 ){
	switch ( $type ) {
    	case 'most_recent' : 
	       $args = array( 
	            'posts_per_page' => $number, 
	            'orderby' => 'date', 
	            'order' => 'DESC',
	         
	        );
	        break;

	    case 'featured' :
	        $args = array( 
	            'posts_per_page' => $number, 
	            'orderby' => 'date', 
	            'order' => 'DESC',
	          
	            'meta_query' => array(
	                    array(
	                        'key'     => 'wpo_postconfig',
	                        'value' => '"is_featured";s:1:"1"',
	                        'compare' => 'like'
	                    ),
	                ),
	            );  
	        break;
	    case 'random' : 
	        $args = array(
	            'post_type' => 'opalhotel_room',
	            'posts_per_page' => $number, 
	            'orderby' => 'rand'
	        );
	        break;
	    default : 
	     	$args = array(
	          
	            'posts_per_page' => $number, 
	            'orderby' => 'rand'
	        );
	        break;
	}

	$args['post_type'] =  'opalhotel_room';
    $args['paged'] = $paged;
	$wp_query = new WP_Query( $args );

	wp_reset_query();
	
	return $wp_query;
}

/**
 * Customizer
 *
 */
add_action( 'customize_register', 'ecopark_fnc_opalhotel_settings' );

function ecopark_fnc_opalhotel_settings( $wp_customize ){

		$wp_customize->add_panel( 'opalhotel_settings', array(
    		'priority' => 70,
    		'capability' => 'edit_theme_options',
    		'theme_supports' => '',
    		'title' => esc_html__( 'OpalHotel Setting', 'ecopark' ),
    		'description' =>esc_html__( 'Make default setting for page, general', 'ecopark' ),
    	) );

        /**
         * Archive Room Page Setting
         */
        $wp_customize->add_section( 'opalhotel_archive_settings', array(
            'priority' => 2,
            'capability' => 'edit_theme_options',
            'theme_supports' => '',
            'title' => esc_html__( 'Archive Room Page Setting', 'ecopark' ),
            'description' => 'Configure archive room page setting',
            'panel' => 'opalhotel_settings',
        ) );

         ///  Archive room layout setting
        $wp_customize->add_setting( 'wpopal_theme_options[opalhotel-archive-layout]', array(
            'capability' => 'edit_theme_options',
            'type'       => 'option',
            'default'   => 'mainright',
            'checked' => 1,
            'sanitize_callback' => 'sanitize_text_field'
        ) );

        $wp_customize->add_control( new Wpopal_Themer_Layout_DropDown( $wp_customize,  'wpopal_theme_options[opalhotel-archive-layout]', array(
            'settings'  => 'wpopal_theme_options[opalhotel-archive-layout]',
            'label'     => esc_html__('Archive Layout', 'ecopark'),
            'section'   => 'opalhotel_archive_settings',
            'priority' => 1

        ) ) );

       //sidebar archive room left
        $wp_customize->add_setting( 'wpopal_theme_options[opalhotel-archive-left-sidebar]', array(
            'capability' => 'edit_theme_options',
            'type'       => 'option',
            'default'   => 'sidebar-left',
            'checked' => 1,
            'sanitize_callback' => 'sanitize_text_field'
        ) );

        $wp_customize->add_control( new Wpopal_Themer_Sidebar_DropDown( $wp_customize,  'wpopal_theme_options[opalhotel-archive-left-sidebar]', array(
            'settings'  => 'wpopal_theme_options[opalhotel-archive-left-sidebar]',
            'label'     => esc_html__('Archive Left Sidebar', 'ecopark'),
            'section'   => 'opalhotel_archive_settings' ,
             'priority' => 3
        ) ) );

          //sidebar archive room right
        $wp_customize->add_setting( 'wpopal_theme_options[opalhotel-archive-right-sidebar]', array(
            'capability' => 'edit_theme_options',
            'type'       => 'option',
            'default'   => 'sidebar-right',
            'sanitize_callback' => 'sanitize_text_field'
        ) );

        $wp_customize->add_control( new Wpopal_Themer_Sidebar_DropDown( $wp_customize,  'wpopal_theme_options[opalhotel-archive-right-sidebar]', array(
            'settings'  => 'wpopal_theme_options[opalhotel-archive-right-sidebar]',
            'label'     => esc_html__('Archive Right Sidebar', 'ecopark'),
            'section'   => 'opalhotel_archive_settings',
             'priority' => 3 
        ) ) );

    	

        /**
    	 * Room Single Setting
    	 */
    	$wp_customize->add_section( 'opalhotel_room_settings', array(
    		'priority' => 3,
    		'capability' => 'edit_theme_options',
    		'theme_supports' => '',
    		'title' => esc_html__( 'Single Room Page Setting', 'ecopark' ),
    		'description' => 'Configure single room page',
    		'panel' => 'opalhotel_settings',
    	) );
        ///  single room layout setting
        $wp_customize->add_setting( 'wpopal_theme_options[opalhotel-single-layout]', array(
            'capability' => 'edit_theme_options',
            'type'       => 'option',
            'default'   => 'mainright',
            'sanitize_callback' => 'sanitize_text_field'
        ) );

        //Select layout
        $wp_customize->add_control( new Wpopal_Themer_Layout_DropDown( $wp_customize,  'wpopal_theme_options[opalhotel-single-layout]', array(
            'settings'  => 'wpopal_theme_options[opalhotel-single-layout]',
            'label'     => esc_html__('Room Detail Layout', 'ecopark'),
            'section'   => 'opalhotel_room_settings',
            'priority' => 1
        ) ) );

       
        $wp_customize->add_setting( 'wpopal_theme_options[opalhotel-single-left-sidebar]', array(
            'capability' => 'edit_theme_options',
            'type'       => 'option',
            'default'   => 1,
            'sanitize_callback' => 'sanitize_text_field'
        ) );

        //Sidebar left
        $wp_customize->add_control( new Wpopal_Themer_Sidebar_DropDown( $wp_customize,  'wpopal_theme_options[opalhotel-single-left-sidebar]', array(
            'settings'  => 'wpopal_theme_options[opalhotel-single-left-sidebar]',
            'label'     => esc_html__('Room Detail Left Sidebar', 'ecopark'),
            'section'   => 'opalhotel_room_settings',
            'priority' => 2 
        ) ) );

        $wp_customize->add_setting( 'wpopal_theme_options[opalhotel-single-right-sidebar]', array(
            'capability' => 'edit_theme_options',
            'type'       => 'option',
            'default'   => 'sidebar-right',
            'sanitize_callback' => 'sanitize_text_field'
        ) );

        //Sidebar right
        $wp_customize->add_control( new Wpopal_Themer_Sidebar_DropDown( $wp_customize,  'wpopal_theme_options[opalhotel-single-right-sidebar]', array(
            'settings'  => 'wpopal_theme_options[opalhotel-single-right-sidebar]',
            'label'     => esc_html__('Room Detail Right Sidebar', 'ecopark'),
            'section'   => 'opalhotel_room_settings',
            'priority' => 3 
        ) ) );

        /**
         * Archive Hotel Page Setting
         */
        $wp_customize->add_section( 'opalhotel_archive_hotel_settings', array(
            'priority' => 5,
            'capability' => 'edit_theme_options',
            'theme_supports' => '',
            'title' => esc_html__( 'Archive Hotel Page Setting', 'ecopark' ),
            'description' => 'Configure archive hotel page setting',
            'panel' => 'opalhotel_settings',
        ) );

         ///  Archive hotel layout setting
        $wp_customize->add_setting( 'wpopal_theme_options[opalhotel-archive-hotel-layout]', array(
            'capability' => 'edit_theme_options',
            'type'       => 'option',
            'default'   => 'mainright',
            'checked' => 1,
            'sanitize_callback' => 'sanitize_text_field'
        ) );

        $wp_customize->add_control( new Wpopal_Themer_Layout_DropDown( $wp_customize,  'wpopal_theme_options[opalhotel-archive-hotel-layout]', array(
            'settings'  => 'wpopal_theme_options[opalhotel-archive-hotel-layout]',
            'label'     => esc_html__('Archive Layout', 'ecopark'),
            'section'   => 'opalhotel_archive_hotel_settings',
            'priority' => 1

        ) ) );

       //sidebar archive hotel left
        $wp_customize->add_setting( 'wpopal_theme_options[opalhotel-archive-hotel-left-sidebar]', array(
            'capability' => 'edit_theme_options',
            'type'       => 'option',
            'default'   => 'sidebar-left',
            'checked' => 1,
            'sanitize_callback' => 'sanitize_text_field'
        ) );

        $wp_customize->add_control( new Wpopal_Themer_Sidebar_DropDown( $wp_customize,  'wpopal_theme_options[opalhotel-archive-hotel-left-sidebar]', array(
            'settings'  => 'wpopal_theme_options[opalhotel-archive-hotel-left-sidebar]',
            'label'     => esc_html__('Archive Left Sidebar', 'ecopark'),
            'section'   => 'opalhotel_archive_hotel_settings' ,
             'priority' => 3
        ) ) );

          //sidebar archive hotel right
        $wp_customize->add_setting( 'wpopal_theme_options[opalhotel-archive-hotel-right-sidebar]', array(
            'capability' => 'edit_theme_options',
            'type'       => 'option',
            'default'   => 'sidebar-right',
            'sanitize_callback' => 'sanitize_text_field'
        ) );

        $wp_customize->add_control( new Wpopal_Themer_Sidebar_DropDown( $wp_customize,  'wpopal_theme_options[opalhotel-archive-hotel-right-sidebar]', array(
            'settings'  => 'wpopal_theme_options[opalhotel-archive-hotel-right-sidebar]',
            'label'     => esc_html__('Archive Right Sidebar', 'ecopark'),
            'section'   => 'opalhotel_archive_hotel_settings',
             'priority' => 3 
        ) ) );

        

        /**
         * Hotel Single Setting
         */
        $wp_customize->add_section( 'opalhotel_hotel_settings', array(
            'priority' => 6,
            'capability' => 'edit_theme_options',
            'theme_supports' => '',
            'title' => esc_html__( 'Single Hotel Page Setting', 'ecopark' ),
            'description' => 'Configure single hotel page',
            'panel' => 'opalhotel_settings',
        ) );
        ///  single hotel layout setting
        $wp_customize->add_setting( 'wpopal_theme_options[opalhotel-single-hotel-layout]', array(
            'capability' => 'edit_theme_options',
            'type'       => 'option',
            'default'   => 'mainright',
            'sanitize_callback' => 'sanitize_text_field'
        ) );

        //Select layout
        $wp_customize->add_control( new Wpopal_Themer_Layout_DropDown( $wp_customize,  'wpopal_theme_options[opalhotel-single-hotel-layout]', array(
            'settings'  => 'wpopal_theme_options[opalhotel-single-hotel-layout]',
            'label'     => esc_html__('Hotel Detail Layout', 'ecopark'),
            'section'   => 'opalhotel_hotel_settings',
            'priority' => 1
        ) ) );

       
        $wp_customize->add_setting( 'wpopal_theme_options[opalhotel-single-hotel-left-sidebar]', array(
            'capability' => 'edit_theme_options',
            'type'       => 'option',
            'default'   => 1,
            'sanitize_callback' => 'sanitize_text_field'
        ) );

        //Sidebar left
        $wp_customize->add_control( new Wpopal_Themer_Sidebar_DropDown( $wp_customize,  'wpopal_theme_options[opalhotel-single-hotel-left-sidebar]', array(
            'settings'  => 'wpopal_theme_options[opalhotel-single-hotel-left-sidebar]',
            'label'     => esc_html__('Hotel Detail Left Sidebar', 'ecopark'),
            'section'   => 'opalhotel_hotel_settings',
            'priority' => 2 
        ) ) );

        $wp_customize->add_setting( 'wpopal_theme_options[opalhotel-single-hotel-right-sidebar]', array(
            'capability' => 'edit_theme_options',
            'type'       => 'option',
            'default'   => 'sidebar-right',
            'sanitize_callback' => 'sanitize_text_field'
        ) );

        //Sidebar right
        $wp_customize->add_control( new Wpopal_Themer_Sidebar_DropDown( $wp_customize,  'wpopal_theme_options[opalhotel-single-hotel-right-sidebar]', array(
            'settings'  => 'wpopal_theme_options[opalhotel-single-hotel-right-sidebar]',
            'label'     => esc_html__('Hotel Detail Right Sidebar', 'ecopark'),
            'section'   => 'opalhotel_hotel_settings',
            'priority' => 3 
        ) ) );

        



	}