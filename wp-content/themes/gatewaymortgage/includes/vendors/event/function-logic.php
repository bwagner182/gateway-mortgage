<?php

function tribe_events_event_location( $event = null ){
	if ( is_null( $event ) ) {
		global $post;
		$event = $post;
	}

	if ( is_numeric( $event ) ) {
		$event = get_post( $event );
	}
	$inner =  '<span class="location">' . tribe_get_city( $event->ID ) . '</span>';
	return $inner;
}

function ecopark_tribe_events_event_schedule_details( $event = null, $before = '', $after = '' ) {
	if ( is_null( $event ) ) {
		global $post;
		$event = $post;
	}
	if ( is_numeric( $event ) ) {
		$event = get_post( $event );
	}
	

	$inner = '<span class="date">'.tribe_get_start_date( $event, false, 'd' ).'</span>';
	$inner .= '<span class="month">'.tribe_get_start_date( $event, false, 'F' ).'</span>';
	return $inner;
}


function ecopark_events_get_the_excerpt( $event = null ){
	if ( is_null( $event ) ) {
		global $post;
		$event = $post;
	}
	if ( is_numeric( $event ) ) {
		$event = get_post( $event );
	}
	echo substr(tribe_events_get_the_excerpt( $event ), 0, 90);
}
add_action( 'tribe_events_list_widget_after_the_meta', 'ecopark_events_get_the_excerpt', 2);

// breadcrumb


function ecopark_fnc_get_event_sidebar_configs( $configs = '' ) {

	$layout =  ecopark_fnc_theme_options( 'event-layout', 'fullwidth');
	$left  	=  ecopark_fnc_theme_options( 'event-left-sidebar', 'event-sidebar-left');
	$right 	=  ecopark_fnc_theme_options( 'event-right-sidebar', 'event-sidebar-right');

	return ecopark_fnc_get_layout_configs($layout, $left, $right, $configs );
}
add_action('ecopark_fnc_get_event_sidebar_configs', 'ecopark_fnc_get_event_sidebar_configs');