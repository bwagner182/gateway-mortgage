<?php 
if( class_exists("WPBakeryShortCode") ){
	/**
	 * Class Ecopark_VC_Woocommerces
	 *
	 */
	class Ecopark_VC_Event  implements Vc_Vendor_Interface  {

		public function load() {
			$columns = array(1, 2, 3, 4, 6);
		    vc_map( array(
			    "name" => esc_html__("PBR Events",'ecopark'),
			    "base" => "pbr_events",
			    "icon" => 'icon-wpb-application-icon-large',
			    "description" =>'Display Event on frontend',
			    "class" => "",
			    "category" => esc_html__('PBR Event', 'ecopark'),
			    "params" => array(
			    	array(
						"type" => "textfield",
						"heading" => esc_html__("Title", 'ecopark'),
						"param_name" => "title",
						"value" => '',
						"admin_label" => true
					),
					array(
						"type" => "dropdown",
						'heading' => esc_html__( 'Order By', 'ecopark' ),
						"param_name" => "orderby",
						"value" => array(
							esc_html__('Featured Events', 'ecopark') => 'featured',
							esc_html__('Lastest Events', 'ecopark') => 'most_recent',
							esc_html__('Randown Events', 'ecopark') => 'random'
						)
				    ),
					array(
						"type" => "textfield",
						'heading' => esc_html__( 'Number', 'ecopark' ),
						"param_name" => "number",
						"value" => ''
				    ),
				    array(
						"type" => "textfield",
						"heading" => esc_html__("Extra class name", 'ecopark'),
						"param_name" => "el_class",
						"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'ecopark')
					)
			   	)
			));
			
		}
	}	

	/**
	  * Register Woocommerce Vendor which will register list of shortcodes
	  */
	function ecopark_fnc_init_vc_event_vendor(){
		$vendor = new Ecopark_VC_Event();
		add_action( 'vc_after_set_mode', array(
			$vendor,
			'load'
		) );

	}
	add_action( 'after_setup_theme', 'ecopark_fnc_init_vc_event_vendor' , 9 );

	class WPBakeryShortCode_pbr_events extends WPBakeryShortCode {}
}		