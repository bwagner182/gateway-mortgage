<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class TG_Social {

	/**
	 * The social platforms for the 
	 * @var array()
	 */
	public $social_platforms = array();

	/**
	 * Set the different social platforms
	 * @access private
	 */
	private function set_social() {
		if( !get_field( 'social_platforms', 'options' ) ) {
			return;
		}

		$platforms = get_field( 'social_platforms', 'options' );

		foreach( $platforms as $social ) {
			$platform = $social['platform'];
			$url = $social['url'];
			$this->social_platforms[$platform] = $url;
		}		

		$this->social_platforms = array_filter( $this->social_platforms );
	}

	/**
	 * Return an array of the Social Platforms
	 * @access public
	 */
	public function get_social_list() {
		$this->set_social();
		return $this->social_platforms;
	}

	/**
	 * Returns the social platforms with HTML markup
	 * @access public
	 */
	public function the_social_list() {
		$this->set_social();
		if( empty( array_filter( $this->social_platforms ) ) ) {
			return;
		}

		$class = 'list--social ';
		$items = array();

		foreach( $this->social_platforms as $key => $value ) {
			$items[] = '<li><a href="' . $value . '" target="_blank"><i class="fa fa-' . $key . '"></i></a></li>';
		}

		//Apply filters
		$class = apply_filters( 'tg_social_list_class', $class );
		$items = apply_filters( 'tg_social_list_items', $items );

		//Build the output
		$output = '<ul class="' . $class . '">';
		foreach( $items as $item ) {
			$output .= $item;
		}
		$output .= '</ul>';

		echo $output;
	}

	public function register_social_meta_box() {
		if( !function_exists( 'acf_add_local_field_group' ) ) {
			return;
		}

		$options_slug = TG()->textdomain . '-options';

		acf_add_local_field_group(array (
			'key' => 'group_5900f8899f588',
			'title' => 'Social Platforms',
			'fields' => array (
				array (
					'key' => 'field_5900f8922a9d9',
					'label' => 'Platforms',
					'name' => 'social_platforms',
					'type' => 'repeater',
					'required' => 0,
					'conditional_logic' => 0,
					'min' => 0,
					'max' => 0,
					'layout' => 'table',
					'button_label' => 'Add Platform',
					'sub_fields' => array (
						array (
							'key' => 'field_5900f8a92a9da',
							'label' => 'Platform',
							'name' => 'platform',
							'type' => 'select',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '20',
							),
							'choices' => array (
								'facebook' => 'Facebook',
								'flickr' => 'Flickr',
								'google-plus' => 'Google',
								'instagram' => 'Instagram',
								'linkedin' => 'LinkedIn',
								'pinterest' => 'Pinterest',
								'reddit' => 'Reddit',
								'tumblr' => 'Tumblr',
								'twitter' => 'Twitter',
								'youtube' => 'YouTube',
								'vine' => 'Vine',
							),
							'default_value' => array (
							),
							'allow_null' => 0,
							'multiple' => 0,
							'ui' => 0,
							'ajax' => 0,
							'return_format' => 'value',
						),
						array (
							'key' => 'field_5900fb780c57b',
							'label' => 'URL',
							'name' => 'url',
							'type' => 'url',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '80',
							),
						),
					),
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'options_page',
						'operator' => '==',
						'value' => $options_slug,
					),
				),
			),
			'menu_order' => 0,
			'position' => 'normal',
			'style' => 'default',
			'label_placement' => 'top',
			'instruction_placement' => 'label',
			'active' => 1,
		));
	}

}

function TG_Social() {
	return new TG_Social();
}