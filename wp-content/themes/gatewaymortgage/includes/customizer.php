<?php
/**
 * mode Customizer support
 *
 * @package WpOpal
 * @subpackage ecopark
 * @since ecopark 1.0
 */

/**
 * Implement Customizer additions and adjustments.
 *
 * @since ecopark 1.0
 *
 * @param WP_Customize_Manager $wp_customize Customizer object.
 */
function ecopark_fnc_customize_register( $wp_customize ) {

    // // / // 
    $wp_customize->add_setting('theme_color', array( 
        'default'    => get_option('theme_color'),
        'type'       => 'option',
        'capability' => 'manage_options',
        'transport'  => 'postMessage',
        'sanitize_callback' => 'sanitize_text_field'
    ) );
    
    $wp_customize->add_control('theme_color', array( 
        'label'    => esc_html__('Theme Color', 'ecopark'),
        'section'  => 'colors',
        'type'      => 'color',
    ) );

    $wp_customize->add_setting('secondary_color', array( 
        'default'    => get_option('secondary_color'),
        'type'       => 'option',
        'capability' => 'manage_options',
        'transport'  => 'postMessage',
        'sanitize_callback' => 'sanitize_text_field'
    ) );
    
    $wp_customize->add_control('secondary_color', array( 
        'label'    => esc_html__('Secondary Theme Color', 'ecopark'),
        'section'  => 'colors',
        'type'      => 'color',
    ) );

    $wp_customize->add_setting('wpopal_theme_options[stickysidebar]', array(
        'capability' => 'edit_theme_options',
        'type'       => 'option',
        'default'   => 0,
        'checked' => 1,
        'sanitize_callback' => 'sanitize_text_field'
    ) );

    $wp_customize->add_control('wpopal_theme_options[stickysidebar]', array(
        'settings'  => 'wpopal_theme_options[stickysidebar]',
        'label'     =>  esc_html__( 'Sticky Sidebar', 'ecopark' ),
        'section'   => 'ts_general_settings',
        'type'      => 'checkbox',
        'transport' => 4,
    ) );

    $wp_customize->add_setting('wpopal_theme_options[fixfooter]', array(
        'capability' => 'edit_theme_options',
        'type'       => 'option',
        'default'   => 0,
        'checked' => 1,
        'sanitize_callback' => 'sanitize_text_field'
    ) );

    $wp_customize->add_control('wpopal_theme_options[fixfooter]', array(
        'settings'  => 'wpopal_theme_options[fixfooter]',
        'label'     =>  esc_html__( 'Fix Footer', 'ecopark' ),
        'section'   => 'ts_general_settings',
        'type'      => 'checkbox',
        'transport' => 4,
    ) );
     
}

add_action( 'customize_register', 'ecopark_fnc_customize_register' );