<?php

if (! defined('ABSPATH')) {
    exit;
}

/**
 * This hook allows for styles to be added to the HEAD tag
 * @param string $styles
 */
function gatewaymortgage_inline_styles($styles)
{
    $styles = apply_filters('gatewaymortgage_inline_styles', $styles);
    $styles = '<style type="text/css">' . $styles . '</style>';

    echo $styles;
}
add_action('wp_head', 'gatewaymortgage_inline_styles');

/*
** Add Favicons
*/
function gatewaymortgage_favicons()
{
    $color = '#ffffff'; //Change this hex value to whatever you would like
    $template_path = get_template_directory_uri().'/images/favicons'; ?>
<link rel="apple-touch-icon" sizes="180x180" href="<?php echo $template_path; ?>/apple-touch-icon.png">
<link rel="icon" type="image/png" href="<?php echo $template_path; ?>/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="<?php echo $template_path; ?>/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="<?php echo $template_path; ?>/manifest.json">
<link rel="mask-icon" href="<?php echo $template_path; ?>/safari-pinned-tab.svg" color="#000000">
<link rel="shortcut icon" href="<?php echo $template_path; ?>/favicon.ico">
<meta name="msapplication-config" content="<?php echo $template_path; ?>/browserconfig.xml">
<meta name="theme-color" content="#291842">
	<?php
}
add_action('wp_head', 'gatewaymortgage_favicons');

function add_file_types_to_uploads($file_types)
{
    $new_filetypes = array();
    $new_filetypes['svg'] = 'image/svg+xml';
    $file_types = array_merge($file_types, $new_filetypes);
    return $file_types;
}
add_filter('upload_mimes', 'add_file_types_to_uploads');
