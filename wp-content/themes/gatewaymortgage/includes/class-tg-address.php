<?php

if (! defined('ABSPATH')) {
    exit;
}

/*
** Create Address Object
*/
class Address
{
    public $street_address;
    public $city;
    public $state;
    public $zip_code;
    public $address_link = null;
    public $post_id = null;
    public $latitude = null;
    public $longitude = null;
    public $repeater = false;

    private $_fields = array(
                'street_address',
                'city',
                'state',
                'zip_code',
                'address_link',
                'latitude',
                'longitude'
            );

    /**
     * Build the Address class
     * @access public
     * @param object||string $post
     * @param bool $repeater
     */
    public function __construct($post = null, $repeater = false)
    {
        if ($repeater) {
            $this->repeater = true;
        }
        $this->set_post_id($post);
        $this->set_address_fields();
    }

    public function init()
    {
    }

    /**
     * Get the Post ID of the gloabl or given post
     * @access private
     * @param 	object||int 	$post
     */
    private function set_post_id($post)
    {
        if ($post === null) {
            global $post;
        }

        if (is_object($post)) {
            $this->post_id = $post->ID;
        } elseif (is_numeric($post)) {
            $this->post_id = $post;
        } elseif (is_string($post)) {
            $this->post_id = 'options';
        }
    }

    /**
     * This sets the variables for this instance of the Address() class
     * @access private
     * @param bool $repeater
     */
    private function set_address_fields($repeater = false)
    {

        //If the funciton is not being used in a repeater field...
        if (!$this->repeater) {
            //.. then use get_field
            foreach ($this->_fields as $field) {
                if (get_field($field, $this->post_id)) {
                    $this->$field = get_field($field, $this->post_id);
                }
            }
        } else {
            //.. otherwise us get_sub_field
            foreach ($this->_fields as $field) {
                if (get_sub_field($field, $this->post_id)) {
                    $this->$field = get_sub_field($field, $this->post_id);
                }
            }
        }
    }

    /**
     * This returns an array of the address data
     * @access public
     */
    public function get_address_fields()
    {
        foreach ($this->_fields as $field) {
            $fields[$field] = $this->$field;
        }

        return array_filter($fields);
        ;
    }

    /**
     * This either echoes or returns the schema marked address
     * @access public
     * @param bool $echo
     */
    public function get_address_schema($echo = true)
    {
        //If we cannot retrieve any address data..
        if (empty(array_filter($this->get_address_fields()))) {
            //.. return nothing
            return;
        }

        //Built the markup properties
        $street_address = '<span itemprop="streetAddress">' . $this->street_address . '</span>';
        $city = '<span itemprop="addressLocality">' . $this->city . '</span>';
        $state = '<span itemprop="addressRegion">' . $this->state . '</span>';
        $zip = '<span itemprop="postalCode">' . $this->zip_code . '</span>';
        $class = '';

        if ($this->longitude && $this->latitude) {
            $lat = 'data-lat="' . $this->latitude . '"';
            $long = 'data-long="' . $this->longitude . '"';
            $data_location = $lat . ' ' . $long;
            $class .= 'js-location ';
        } else {
            $data_location = null;
        }
        $class = apply_filters('gatewaymortgage_address_class', $class);

        // Construst the output
        $address = '<address class="' . $class . '" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress" ' . $data_location . '>';
        if ($this->address_link) {
            $address .= '<a href="' . $this->address_link . '" class="ga-address" target="_blank">';
        }
        $address .= $street_address . ' ' . $city . ', ' . $state . ' ' . $zip;
        if ($this->address_link) {
            $address .= '</a>';
        }
        $address .= '</address>';

        // If echo is true..
        if ($echo) {
            // .. echo the output
            echo $address;
        } else {
            // .. else return the value
            return $address;
        }
    }
}
