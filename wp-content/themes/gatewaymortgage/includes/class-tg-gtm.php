<?php
/*
** Set GTM_CODE to the property string to implement GTM
*/
if (! defined('ABSPATH')) {
    exit;
}

class TG_GTM
{

    /**
    * @var string $gtm
    */
    public $gtm;

    public function __construct($gtm_code = null)
    {
        if (!$gtm_code) {
            return;
        } else {
            $this->gtm = $gtm_code;
        }

        $this->init();
    }

    public function init()
    {
        add_action('wp_head', array( $this, 'gtm_header' ), 0);
        add_action('gatewaymortgage_after_body_start', array( $this, 'gtm_body' ), 0);
    }

    public function gtm_header()
    {
        ?>
            <!-- Google Tag Manager -->
            <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-<?php echo $this->gtm; ?>');</script>
            <!-- End Google Tag Manager -->
		<?php
    }

    public function gtm_body()
    {
        ?>
            <!-- Google Tag Manager (noscript) -->
            <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-<?php echo $this->gtm; ?>"
            height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
            <!-- End Google Tag Manager (noscript) -->
		<?php
    }
}
