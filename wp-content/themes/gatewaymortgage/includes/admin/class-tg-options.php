<?php

if (! defined('ABSPATH')) {
    exit;
}

class TG_Options
{
    public static function init()
    {
        self::set_options_page();
        self::create_nav_menus();
    }

    /**
     * Create the Options Page
     */
    public function set_options_page()
    {
        $options_page_name = TG()->common_name . ' Options';
        $options_slug = TG()->textdomain . '-options';

        if (function_exists('acf_add_options_page')) {
            acf_add_options_page(
                array(
                    'page_title' 	=> $options_page_name,
                    'menu_slug' 	=> $options_slug
                )
            );
        }
    }

    /**
     * Create Menus
     * @uses register_nav_menus()
     */
    public function create_nav_menus()
    {
        register_nav_menus(
            array(
            'primary' => esc_html__('Primary Menu', TG()->textdomain),
            'mobile-1' => esc_html__('Get Started (mobile)', TG()->textdomain),
            'mobile-2' => esc_html__('Company (mobile)', TG()->textdomain),
            'mobile-3' => esc_html__('Resources (mobile)', TG()->textdomain),
            )
        );
    }
}

TG_Options::init();
