<?php
if (! defined('ABSPATH')) {
    exit;
}

class TG_Widgets
{
    public function init()
    {
        /*register_sidebar(array(
            'name'          => esc_html__('Sidebar', 'gatewaymortgage'),
            'id'            => 'sidebar-1',
            'description'   => '',
            'before_widget' => '<aside id="%1$s" class="widget %2$s">',
            'after_widget'  => '</aside>',
            'before_title'  => '<h4 class="widget-title">',
            'after_title'   => '</h4>',
        ));*/
        register_sidebar(array(
            'name'          => esc_html__('Footer 1', 'gatewaymortgage'),
            'id'            => 'footer-1',
            'description'   => '',
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget'  => '</div>',
            'before_title'  => '<h4 class="widget-title">',
            'after_title'   => '</h4>',
        ));
        register_sidebar(array(
            'name'          => esc_html__('Footer 2', 'gatewaymortgage'),
            'id'            => 'footer-2',
            'description'   => '',
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget'  => '</div>',
            'before_title'  => '<h4 class="widget-title">',
            'after_title'   => '</h4>',
        ));
        register_sidebar(array(
            'name'          => esc_html__('Footer 3', 'gatewaymortgage'),
            'id'            => 'footer-3',
            'description'   => '',
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget'  => '</div>',
            'before_title'  => '<h4 class="widget-title">',
            'after_title'   => '</h4>',
        ));
        register_sidebar(array(
            'name'          => esc_html__('Footer 4', 'gatewaymortgage'),
            'id'            => 'footer-4',
            'description'   => '',
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget'  => '</div>',
            'before_title'  => '<h4 class="widget-title">',
            'after_title'   => '</h4>',
        ));

        register_sidebar(array(
            'name'          => esc_html__('Footer 5', 'gatewaymortgage'),
            'id'            => 'footer-5',
            'description'   => '',
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget'  => '</div>',
            'before_title'  => '<h4 class="widget-title">',
            'after_title'   => '</h4>',
        ));
    }
}
