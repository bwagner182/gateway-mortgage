<?php

class TG_Taxonomy {

	private $labels;

	private $args;

	private $handle;

	private $post_type;

	public function __construct( $handle, $args ) {
		if( empty( $args ) ) {
			return;
		}

		$this->set_taxonomy_vars( $handle, $args );
		add_action( 'init', array( $this, 'register_taxonomy' ), 0 );

	}

	private function set_taxonomy_vars( $handle, $args ) {
		$this->post_type = $args['post_type'];
		$this->handle = strtolower( str_replace( '-', '_', $handle ) );
		$this->args = $args['args'];
		$this->labels = $args['labels'];
	}

	public function register_taxonomy() {
		$labels = array(
			'name'                       => _x( $this->labels['name'], 'Taxonomy General Name', TG()->textdomain ),
			'singular_name'              => _x( $this->labels['singular_name'], 'Taxonomy Singular Name', TG()->textdomain ),
			'menu_name'                  => __( $this->labels['name'], TG()->textdomain ),
			'all_items'                  => __( 'All Items', TG()->textdomain ),
			'parent_item'                => __( 'Parent Item', TG()->textdomain ),
			'parent_item_colon'          => __( 'Parent Item:', TG()->textdomain ),
			'new_item_name'              => __( 'New Item Name', TG()->textdomain ),
			'add_new_item'               => __( 'Add New Item', TG()->textdomain ),
			'edit_item'                  => __( 'Edit Item', TG()->textdomain ),
			'update_item'                => __( 'Update Item', TG()->textdomain ),
			'view_item'                  => __( 'View Item', TG()->textdomain ),
			'separate_items_with_commas' => __( 'Separate items with commas', TG()->textdomain ),
			'add_or_remove_items'        => __( 'Add or remove items', TG()->textdomain ),
			'choose_from_most_used'      => __( 'Choose from the most used', TG()->textdomain ),
			'popular_items'              => __( 'Popular Items', TG()->textdomain ),
			'search_items'               => __( 'Search Items', TG()->textdomain ),
			'not_found'                  => __( 'Not Found', TG()->textdomain ),
			'no_terms'                   => __( 'No items', TG()->textdomain ),
			'items_list'                 => __( 'Items list', TG()->textdomain ),
			'items_list_navigation'      => __( 'Items list navigation', TG()->textdomain ),
		);
		$args = array(
			'labels'                     => $labels,
			'hierarchical'               => true,
			'public'                     => true,
			'show_ui'                    => true,
			'show_admin_column'          => true,
			'show_in_nav_menus'          => false,
			'show_tagcloud'              => true,
		);

		if( $this->args['hierarchical'] != true ) {
			$args['hierarchical'] = $this->args['hierarchical'];
		}

		if( $this->args['show_in_nav_menus'] != false ) {
			$args['show_in_nav_menus'] = $this->args['show_in_nav_menus'];
		}

		register_taxonomy( $this->handle, array( $this->post_type ), $args );
	}

}