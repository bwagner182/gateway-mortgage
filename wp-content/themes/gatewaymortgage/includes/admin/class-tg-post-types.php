<?php

class TG_Post_Type {

	private $labels;

	private $slug;

	private $handle;

	private $rewrite = '';

	private $args;

	public function __construct( $handle, $args ) {
		if( empty( $args ) ) {
			return;
		}

		$this->set_post_type_vars( $handle, $args );
		add_action( 'init', array( $this, 'register_post_type' ), 5 );
	}

	private function set_post_type_vars( $handle, $args ) {
		$this->labels = $args['labels'];
		$this->args = $args['args'];
		$this->slug = strtolower( str_replace( ' ', '-', $this->labels['singular'] ) );
		$this->handle = str_replace( '-', '_', $handle );

		if( empty( $args['rewrite'] ) ) {
			$this->rewrite = strtolower( str_replace( ' ', '-', $this->labels['plural'] ) );
		} elseif( is_string( $args['rewrite'] ) ) {
			$this->rewrite = strtolower( $args['rewrite'] );
		}
	}



	public function register_post_type() {

		if( !empty( $this->rewrite ) ) {
			$rewrite = array(
				'slug'                => $this->rewrite,
				'with_front'          => true,
				'pages'               => true,
				'feeds'               => true,
			);
		}

		$labels = array(
			'name'                => _x( $this->labels["plural"], 'Post Type General Name', TG()->textdomain ),
			'singular_name'       => _x( $this->labels["singular"], 'Post Type Singular Name', TG()->textdomain ),
			'menu_name'           => __( $this->labels["plural"], TG()->textdomain ),
			'name_admin_bar'      => __( $this->labels["singular"], TG()->textdomain ),
			'parent_item_colon'   => __( 'Parent Item:', TG()->textdomain ),
			'all_items'           => __( 'All ' . $this->labels["plural"], TG()->textdomain ),
			'add_new_item'        => __( 'Add New ' . $this->labels["singular"], TG()->textdomain ),
			'add_new'             => __( 'Add New', TG()->textdomain ),
			'new_item'            => __( 'New ' . $this->labels["singular"], TG()->textdomain ),
			'edit_item'           => __( 'Edit ' . $this->labels["singular"], TG()->textdomain ),
			'update_item'         => __( 'Update ' . $this->labels["singular"], TG()->textdomain ),
			'view_item'           => __( 'View ' . $this->labels["singular"], TG()->textdomain ),
			'search_items'        => __( 'Search ' . $this->labels["plural"], TG()->textdomain ),
			'not_found'           => __( 'Not found', TG()->textdomain ),
			'not_found_in_trash'  => __( 'Not found in Trash', TG()->textdomain ),
		);		
		$args = array(
			'label'               => __( $this->labels["singular"], TG()->textdomain ),
			'description'         => __( $this->labels["plural"], TG()->textdomain ),
			'labels'              => $labels,
			'supports'            => $this->args['supports'],
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'menu_position'       => 5,
			'menu_icon'           => $this->args['menu-icon'],
			'show_in_admin_bar'   => true,
			'show_in_nav_menus'   => true,
			'can_export'          => true,
			'has_archive'         => $this->args['archive'],		
			'exclude_from_search' => false,
			'publicly_queryable'  => true,
			'capability_type'     => 'page',
		);

		if( $rewrite ) {
			$args['rewrite'] = $rewrite;
		}

		if( $this->args['taxonomies'] ) {
			$args['taxonomies'] = $this->args['taxonomies'];
		}
		

		register_post_type( $this->handle, $args );
	}
}