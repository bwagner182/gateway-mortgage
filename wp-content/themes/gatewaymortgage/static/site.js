jQuery( document ).ready( function( $ ) {

  	// Your JavaScript goes here

	$("a.video-popup").YouTubePopUp();

	var swiper = new Swiper('.swiper-container', {
		pagination: {
			el: '.swiper-pagination',
			type: 'bullets'
		},
		init: true,
		direction: 'horizontal',
		touchEventsTarget: 'container',
		initialSlide: 0,
		speed: 300,
		slidesPerView: 1,
		spaceBetween: 60,
		loop: true,
		threshold: 50,
		autoplay: {
			delay: 6000
		}
	});

});

function navigationToggle() {
	var menu = document.getElementById('mobile-navigation');
	var classList = document.getElementById('mobile-navigation').className.split(/\s+/);

    if (classList.includes('show')) {
    	// console.log('close menu');
       	menu.classList.remove('show');

       	swapNavButtonIcon();
    } else {
    	// console.log('open menu');
    	menu.classList.add('show');
    	
    	swapNavButtonIcon();
    }

}

function swapNavButtonIcon() {
	var icon = jQuery("button.navbar-toggler .fad");
	var classes = jQuery("button.navbar-toggler .fad").attr('class').split(/\s+/);
		
	if(classes.includes('fa-bars')) {
		icon.removeClass('fa-bars');
		icon.addClass('fa-times');
	} else if (classes.includes('fa-times')) {
		icon.removeClass('fa-times');
		icon.addClass('fa-bars');
	}
}
