<?php
/**
 * The template for displaying all single posts.
 *
 * @package gatewaymortgage
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php
                $post_type = get_post_type();

                if ('post' == $post_type) :
                    while (have_posts()) : the_post(); ?>			
			<?php
                    get_template_part('page-templates/parts/content', 'single');


                    // If comments are open or we have at least one comment, load up the comment template
                    if (comments_open() || get_comments_number()) :
                            comments_template();
                    endif;


                    endwhile; // end of the loop.
                elseif ('video' == $post_type) :
                    while (have_posts()) : the_post();
                    get_template_part('page-templates/parts/content', 'video'); ?>
                    
                    <?php

                    the_post_navigation();

                        // If comments are open or we have at least one comment, load up the comment template
                        if (comments_open() || get_comments_number()) :
                                comments_template();
                        endif;


                    endwhile; // end of the loop.
                endif;
                ?>

                    <div class="container">
        <?php
   // the query
   $the_query = new WP_Query(array(
     'category_name' => wp_get_post_categories()[0],
      'posts_per_page' => 3,
   ));
?>

<?php if ($the_query->have_posts()) : ?>
  <div class="blog-posts-wrapper">
    <div class="row no-pad-top no-pad-bottom justify-content-center">
        <div class="col-md-6">
            <h3 class="black center">Other Resources</h3>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row no-pad-top">
        <?php
        while ($the_query->have_posts()) : $the_query->the_post(); ?>
            <div class="col-sm-12 col-md-4">
                <div class="blog-item">
                    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">  
                        <div class="blog-item-inner">
                            <div class="featured-image">
                                <?php the_post_thumbnail('blog-thumbnail'); ?>
                            </div>
                            <div class="blog-info-wrapper">
                                <h4 class="post-title"><?php the_title(); ?></h4>
                                <p class="post-excerpt"><?php echo wp_trim_excerpt(); ?></p>
                                <a class="inline-link" href="<?php the_permalink()?>" title="<?php the_title(); ?>">Read More</a>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        <?php
        endwhile; ?>
    </div>
</div>
  <?php wp_reset_postdata(); ?>

<?php else : ?>
  <p><?php __('No News'); ?></p>
<?php endif; ?>
    </div>

    <?php get_template_part('page-templates/parts/content-bottom'); ?>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
