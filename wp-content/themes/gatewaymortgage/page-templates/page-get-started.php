<?php
/**
 * Template Name: Request a Quote
 */

get_header(); ?>
<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<section class="white-bg">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-md-10 col-lg-8">
						<h1 class="page-title black center"><?php the_field('page_header');?></h1>
						<p class="center"><?php the_field('page_content');?></p>
						<?php gravity_form(1, false, false);?>
					</div>
				</div>
			</div>
		</section>
		<section class="grey-bg">
			<div class="container">
				<div class="row flex-row align-items-center justify-content-center">
					<div class="col-md-4 purple-bg" id="call-us">
						<div class="call-col-inner">
							<p class="center"><i class="fad fa-headset fa-3x"></i></p>
							<p class="center white">Call Us</p>
							<p class="phone-link center white"><a href="tel:+1<?php echo str_replace(' ', '-', get_field('phone_number'));?>"><?php the_field('phone_number');?></a></p>
						</div>
					</div>
					<div class="col-md-4" id="address-col">
						<div class="address-col-inner">
							<p class="center"><i class="fad fa-map-marker-alt fa-3x"></i></p>
							<p class="black center">Find Us</p>
							<p class="center black address-link">
								<?php
                                if (!is_null(get_field('address_link'))) {
                                    $link = get_field('address_link'); ?>
									<a href="<?php echo $link['url']; ?>" title="Get Directions">
										<?php
                                }
                                    
                                    the_field('address');
                                ?>

								<?php
                                if (!is_null(get_field('address_link'))) {
                                    ?>
									</a>
								<?php
                                }
                                ?>
							</p>
					</div>
				</div>
			</div>
		</section>
	</main>
</div>
<?php
get_footer();
?>
