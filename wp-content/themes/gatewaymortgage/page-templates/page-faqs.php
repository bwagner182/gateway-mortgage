<?php
/**
 * Template Name: FAQs
 */

get_header(); ?>
<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<div class="container">
			<div class="row">
				<div class="col-md-8">
					<div class="faq-header">
						<h1 class="page-title black"><?php the_field('page_header');?></h1>
						<h2 class="small"><?php the_field('subheader');?></h2>
					</div>
					<?php
                    if (have_rows('faqs')) {
                        ?>
						<div class="faqs-wrapper">
							<?php
                            while (have_rows('faqs')) : the_row(); ?>
								<div class="faq-item">
									<p class="faq-question"><?php the_sub_field('question'); ?></p>
									<p class="faq-answer"><?php the_sub_field('answer'); ?></p>
								</div>
								<?php
                            endwhile; ?>
						</div>
						<?php
                    }
                    ?>
				</div>
				<div class="col-md-4 sidebar sidebar sidebar-faq">
					<div class="faq-widget">
						<div class="widget-inner">
							<h3 class="small">Have another question?</h3>
							<p>Connect with one of our licensed mortgage specialist today.</p>
							<a class="btn btn-green btn-large" href="/request-a-quote/" title="Contact us today">Contat Us</a>
						</div>
					</div>
				</div>
			</div>
		</div>


		<?php get_template_part('page-templates/parts/content-bottom');?>

	</main>
</div>
<?php
get_footer();
?>
