<?php
/**
 * Template Name: Thank you
 */

get_header(); ?>
<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<section class="container-fluid purple-bg fullscreen-section align-items-center justify-content-center">
			<div class="container">
				<div class="row flex-row align-items-center justify-content-center">
					<div class="col-md-10 col-lg-8">
						<h1 class="page-title white center">Sent!</h1>
						<h2 class="white center h3">Thank you for your interest in Gateway Mortgage!</h2>
						<p class="white center">We will be in contact with you soon to discuss your unique needs and get you started to finding the perfect loan. If you have any questions, feel free to explore our frequently asked questions.</p>
						<p class="btn-wrapper btn-center flex-btn center">
							<a class="btn btn-white-underline" href="/faqs/" title="View our FAQ page">FREQUENTLY ASKED QUESTIONS</a>
						</p>
						<p class="btn-wrapper btn-center flex-btn center">
							<a class="btn btn-white-underline" href="/" title="Return to homepage">Return to homepage</a>
						</p>
					</div>
				</div>
			</div>
		</section>
	</main>
</div>
<?php
get_footer();
?>
