<?php
/**
 * Template Name: About Us
 */

get_header(); ?>
<div class="jumbotron hero__about-us container-fluid" style="background: linear-gradient(to bottom, rgba(0,0,0,0.6), rgba(0,0,0,0.6)), url(<?php echo esc_url(get_field('background_image'));?>); background-size: cover; background-repeat: no-repeat;background-position-y: 20%;">
	<div class="container">
		<div class="row align-items-end">
			<div class="col-md-10 col-lg-6 offset-md-1 offset-lg-2">
				<h1 class="hero-title"><?php the_field('hero_headline');?></h1>
			</div>
		</div>
	</div>
</div>
<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<section class="white-bg">
			<div class="container">
				<div class="row flex-row align-tems-center justify-content-center">
					<div class="col-md-8">
						<h2 class="section-title"><?php the_field('content_header');?></h2>
						<?php the_field('content_body');?>

						<?php
                        if (have_rows('testimonials')) {
                            ?>
								
                        	<div class="row-inner">
                        		<div class="testimonials">
									<div class="testimonial-wrapper swiper-container">
										<div class="swiper-wrapper">
							<?php
                            while (have_rows('testimonials')) : the_row(); ?>
						
									<div class="testimonial-slide swiper-slide">
										<p class="testimonial">"<?php the_sub_field('quote'); ?>"</p>
									</div>
									
								<?php
                            endwhile; ?>
                        				</div>

				                    <div class="swiper-pagination"></div>

				                    </div>
				                    
                            	</div>
		                    </div>
		                 	
                        <?php
                        } // end testimonials if statement
                        ?>
					</div>
				</div>
			</div>
		</section>


		<?php get_template_part('page-templates/parts/content-bottom');?>

	</main>
</div>
<?php
get_footer();
?>
