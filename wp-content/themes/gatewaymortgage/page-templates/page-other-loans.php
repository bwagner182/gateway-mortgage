<?php
/**
 * Template Name: Other Loan Options
 */

get_header(); ?>
<div class="jumbotron hero__loan-options container-fluid" style="background: linear-gradient(to bottom, rgba(0,0,0,0.3), rgba(0,0,0,0.3)), url(<?php echo esc_url(get_field('background_image'));?>); background-size: cover; background-repeat: no-repeat; background-position-y: 45%;">
	<div class="container">
		<div class="row align-items-center justify-content-center">
			<div class="col-md-7 col-lg-5">
				<h1 class="hero-title center white"><?php the_title();?></h1>
				
			</div>
		</div>
	</div>
</div>
<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<div class="container two-column-section">
			<div class="row page-content">
				<div class="col-md-8">
					<h2 class="small"><?php the_field('content_header');?></h2>

					<?php
                    if (have_rows('loan_options')) {
                        ?>
						<div class="loan-options-wrapper">
							<?php
                            while (have_rows('loan_options')) : the_row();
                        $id = str_replace(' ', '-', strtolower(get_sub_field('loan_option_name'))); ?>
								<div class="loan-option" id="<?php echo $id; ?>">
									<h3 class="loan-title"><?php the_sub_field('loan_option_name'); ?></h3>
									<p class="loan-details"><?php the_sub_field('loan_details'); ?></p>
								</div>
								<?php
                            endwhile; ?>
						</div>
						<?php
                    }
                    ?>
				</div>
				<div class="col-md-4">
					<div class="loan-widget">
						<div class="widget-inner">
							<h3 class="small">Have another question?</h3>
							<p>Connect with one of our licensed mortgage specialist today.</p>
							<a class="btn btn-green btn-large" href="/contact/" title="Contact us today">Contat Us</a>
						</div>
					</div>
				</div>
			</div>
		</div>


		<?php
        get_template_part('page-templates/parts/content-bottom');
        ?>
	</main>
</div>
<?php
get_footer();
?>
