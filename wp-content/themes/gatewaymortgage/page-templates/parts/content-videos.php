<?php
/**
 * @package gatewaymortgage
 */
?>

<div class="container">
	
	<?php if (have_posts()): ?>
		<div class="row flex-row" id="video-posts-list">
		<?php while (have_posts()) : the_post(); ?>
			<?php
                $fields = get_fields();
                $video_link = $fields['video_link'];
            ?>

            <div class="col-sm-12 col-md-6 col-lg-4 video-post">
                <div class="post-inner">	
                	<a class="video-popup" href="<?php echo $video_link;?>" >
                		<div class="image_wrapper">
                			<?php the_post_thumbnail(600, 400);?>
                		</div>
                	</a>
                	<h2 class="video-title"><?php the_title();?></h2>
                	<?php
                    if (get_field("guest_star") && get_field("guest_star") != "") {
                        ?>
                		<p class="guest-star">Special Guest: <a href="<?php the_field("guest_star_link"); ?>" title="<?php the_field("guest_star"); ?>"><?php the_field("guest_star"); ?></a></p>
                		<?php
                    }
                    ?>
                </div>
            </div>
            
		<?php endwhile;?>
		
		</div>
		<div class="row flex-row justify-content-center">
			<div class="col-md-10 col-lg-8">
				<?php
                // the_posts_navigation(array('screen_reader_text'=>"View More Posts"));
                the_posts_pagination();
                ?>		  	
			</div>
		</div>

	<?php endif;?>
			
	
</div>

<?php get_template_part('page-templates/parts/content-bottom');?>
