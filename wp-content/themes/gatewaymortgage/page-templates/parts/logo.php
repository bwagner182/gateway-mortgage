<?php
/**
 * Header Logo Wrapper
 */
?>
<div class="col-sm-12 col-lg-2 navbar-left">
	<div class="logo-wrapper">		
		<a href="<?php echo get_bloginfo('siteurl');?>" title="<?php echo get_bloginfo('name');?>">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/static/img/logo__full-color.svg" alt="<?php echo get_bloginfo('name');?>" class="gateway-logo"/>
		</a>
	</div>
</div>
