<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package gatewaymortgage
 */
?>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <header class="entry-header">
                        <h1 class="entry-title page-title black"><?php the_title(); ?> for <?php echo get_bloginfo('site-url');?></h1>
                    </header>
                    <!-- .entry-header -->
                    <div class="entry-content">
                        <?php the_content(); ?>
                    </div>
                    <!-- .entry-content -->
                    <footer class="entry-footer">
                        <?php edit_post_link(esc_html__('Edit', 'gatewaymortgage'), '<span class="edit-link">', '</span>'); ?>
                    </footer>
                    <!-- .entry-footer -->
                </article>
                <!-- #post-## -->
            </div>
        </div>
    </div>
