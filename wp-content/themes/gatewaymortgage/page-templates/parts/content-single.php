<?php
/**
 * @package gatewaymortgage
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<div class="jumbotron hero__single" style="background-image: linear-gradient(to bottom, rgba(0,0,0,0.6), rgba(0,0,0,0.6)), url('<?php echo get_the_post_thumbnail_url();?>');">
			<div class="container">
				<div class="row flex-row justify-content-center align-tems-center">
					<div class="col-lg-10">
						<?php the_title('<h1 class="entry-title center">', '</h1>'); ?>
						<div class="entry-meta center">
							<?php gatewaymortgage_posted_on(); ?>
						</div><!-- .entry-meta -->
					</div>
				</div>
			</div>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-lg-8" id="post-content">
					<?php the_content(); ?>
				</div>

				<div class="col-md-12 col-lg-4 sidebar sidebar-single">
					<div class="single-widget">
						<div class="widget-inner">
							<h3 class="small">Expert advice at your fingertips.</h3>
							<p>Connect with one of our licensed mortgage specialist today.</p>
							<a class="btn btn-green btn-large" href="/request-a-quote/" title="Contact us today">Contat Us</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div><!-- .entry-content -->

	<div class="container">
		<?php gatewaymortgage_entry_footer();?>
	</div><!-- .entry-footer -->
	

	
</article><!-- #post-## -->
