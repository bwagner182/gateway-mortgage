<?php
/**
 * Navigation Parts for mobile
 */

?>

<div class="collapse navbar-collapse" id="mobile-navigation">
	<div class="row">
		<div class="col-sm-6">
		  	<h3 class="menu-header">Get Started</h3>
		  	<?php
            wp_nav_menu(array(
                'menu'              => 'mobile-1',
                'theme_location'    => 'mobile-1',
                'menu_id'			=> 'mobile-get-started',
                'depth'             => 2,
                'container'         => 'nav',
                //'container_class'   => 'collapse navbar-collapse flex-row',
                'container_id'      => 'main-nav',
                'menu_class'        => 'nav navbar-nav flex-column ml-auto',
                'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                'walker'            => new WP_Bootstrap_Navwalker()
            ));
            ?>
		</div>
		<div class="col-sm-6">
		  	<h3 class="menu-header">Company</h3>
		  	<?php
            wp_nav_menu(array(
                'menu'              => 'mobile-2',
                'theme_location'    => 'mobile-2',
                'menu_id'			=> 'mobile-company',
                'depth'             => 2,
                'container'         => 'nav',
                //'container_class'   => 'collapse navbar-collapse flex-row',
                'container_id'      => 'main-nav',
                'menu_class'        => 'nav navbar-nav flex-column ml-auto',
                'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                'walker'            => new WP_Bootstrap_Navwalker()
            ));
            ?>
		</div>
		<div class="col-sm-6">
		  	<h3 class="menu-header">Resources</h3>
		  	<?php
            wp_nav_menu(array(
                'menu'              => 'mobile-3',
                'theme_location'    => 'mobile-3',
                'menu_id'			=> 'mobile-resources',
                'depth'             => 2,
                'container'         => 'nav',
                //'container_class'   => 'collapse navbar-collapse flex-row',
                'container_id'      => 'main-nav',
                'menu_class'        => 'nav navbar-nav flex-column ml-auto',
                'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                'walker'            => new WP_Bootstrap_Navwalker()
            ));
            ?>
		</div>
		<div class="col-sm-6">
		  	<h3 class="menu-header">Connect With Us</h3>
		  	<div class="social-row">
		  		<a href="#" title="Like us on facebook"><i class="fab fa-facebook-f "></i></a>
		  		<a href="#" title="Follow us on Instagram"><i class="fab fa-instagram "></i></a>
		  		<a href="#" title="Follow us on LinkedIn"><i class="fab fa-linkedin "></i></a>
		  	</div>
		</div>
	</div>
</div>
