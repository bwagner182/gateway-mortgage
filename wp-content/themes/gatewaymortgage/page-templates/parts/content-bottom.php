<section class="content-bottom image-bg">
	<div class="container-fluid">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-10 col-lg-6">
					<h2 class="section-title white">Expert advice at your fingertips.</h2>
					<p class="white">Give us a call today to speak to a licensed mortgage specialist.</p>
					<div class="row-inner align-items-center">
						<div class="col-sm-12 col-md-5">
							<a class="btn btn-white" href="tel://+13148223999" title="Call us today">314-822-3999</a>
						</div>
						<div class="col-sm-12 col-md-6">
							<div class="underline-link-wrapper">
								<a class="inline-link underline underline-lro2 white" href="/request-a-quote/" title="Get Started">Get Started</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
