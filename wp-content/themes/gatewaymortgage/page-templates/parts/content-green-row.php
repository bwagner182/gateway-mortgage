<section class="green-bg">
			<div class="container-fluid">
				<div class="container">
					<div class="row no-pad-bottom justify-content-center">
						<?php
                        // Get green row data from WordPress Post
                        $row = get_field('green_row');
                        ?>
						<div class="col-sm-12 col-md-10 col-lg-8">
							<h2 class="section-title white"><?php echo $row['row_headline'];?></h2>
							<p class="white"><?php echo $row['row_content']?></p>
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="row align-items-center no-pad-top">
						<div class="col-md-6 col-lg-3">
							<div class="cta-link-wrapper">
								<div class="cta-icon-link" href="/contact/">
									<i class="fas fa-phone-square-alt"></i>
									<span>Contact Us</span>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-lg-3">
							<div class="cta-link-wrapper">
								<div class="cta-icon-link" href="#">
									<i class="fas fa-check-circle"></i>
									<span>Get Pre-Approved</span>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-lg-3">
							<div class="cta-link-wrapper">
								<div class="cta-icon-link" href="#">
									<i class="fas fa-home"></i>
									<span>Find a Dream Home</span>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-lg-3">
							<div class="cta-link-wrapper">
								<div class="cta-icon-link" href="">
									<i class="fas fa-box-open"></i>
									<span>Close & Move-In</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
