<?php
/**
 * Navigation Parts
 */
?>

<div class="col-sm-12 col-lg-10 navbar-right">
    <div class="flex-row justify-content-end">
        <div class="nav-wrapper">	
            <?php
            wp_nav_menu(array(
                'menu'              => 'primary',
                'theme_location'    => 'primary',
                'depth'             => 2,
                'container'         => 'nav',
                'container_class'   => 'collapse navbar-collapse flex-row',
                'container_id'      => 'main-nav',
                'menu_class'        => 'nav navbar-nav flex-row ml-auto',
                'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                'walker'            => new WP_Bootstrap_Navwalker()
            ));
            ?>
        </div>
        <div class="md-hidden contact-cta">
            <div class="flex-row align-items-center">
                <div class="col-lg-6">
                    <p class="center black">
                        <a href="tel:+13148223999" title="call now">(314) 822-3999</a>
                    </p>
                </div>
                <div class="col-lg-6">
                    <p class="btn-wrapper center btn-center">
                        <a class="btn btn-green" title="Apply for a loan" href="/request-a-quote">Get Started</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
		
