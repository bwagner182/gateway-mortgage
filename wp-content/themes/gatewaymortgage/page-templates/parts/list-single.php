<?php
/**
 * Template Name: Blog Archive
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package gatewaymortgage
 */
?>

<?php if (have_posts()) : ?>

	<div class="container">
		<div class="blog-list-wrapper">
			<div class="row flex-column align-items-center">

		<?php
        $post_type = get_post_type();

        ?>
        <script>console.log("post type: <?php echo $post_type;?>");</script>
		<?php /* Start the Loop*/  ?>
		<?php while (have_posts()) : the_post(); ?>

			<div class="col-md-10 col-lg-8">
				<a href="<?php the_permalink();?>" title="<?php the_title();?>" class="post-link">
					<div class="col-inner">
						<div class="post-wrapper">
							<h2 class="post-title"><?php the_title();?></h2>
							<?php gatewaymortgage_posted_on();?>
							<?php the_excerpt();?>
						</div>
					</div>
				</a>
			</div>

		<?php endwhile; ?>

		<?php the_posts_navigation(); ?>

<?php else : ?>

		<?php get_template_part('page-templates/parts/content', 'none'); ?>
			</div>
		</div>
	</div>

<?php endif; ?>

