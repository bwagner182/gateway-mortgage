<?php
/**
 * Template Name: Homepage
 */

get_header(); ?>
<div class="jumbotron hero__home container-fluid" style="background: linear-gradient(to bottom, rgba(0,0,0,0.6), rgba(0,0,0,0.6)), url(<?php echo esc_url(get_field('background_image'));?>); background-size: cover; background-repeat: no-repeat;">
	<div class="container">
		<div class="row align-items-center">
			<div class="col-md-7 col-lg-5">
				<h1 class="hero-title"><?php the_field('hero_headline');?></h1>
				<p class="hero-content"><?php the_field('hero_text');?></p>
				<div class="clearfix"></div>
				<div class="row no-pad-top no-pad-bottom">
					<div class="col-md-6">
						<a class="btn btn-white btn-large" href="<?php echo get_field('left_button')['url'];?>" title="<?php echo get_field('left_button')['title'];?>" target="<?php echo get_field('left_button')['target'] ? get_field('left_button')['target'] : '_self';?>"><?php echo get_field('left_button')['title'];?></a>
					</div>
					<div class="col-md-6">
						<a class="btn btn-white btn-large" href="<?php echo get_field('right_button')['url'];?>" title="<?php echo get_field('right_button')['title'];?>" target="<?php echo get_field('right_button')['target'] ? get_field('right_button')['target'] : '_self';?>"><?php echo get_field('right_button')['title'];?></a>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="row no-pad-top no-pad-bottom">
					<div class="col-sm-12">
						<div class="underline-link-wrapper">	
							<a class="hero-bottom-link underline underline-lro2 yellow" href="<?php echo get_field('bottom_link')['url'];?>" title="<?php echo get_field('bottom_link')['title'];?>"><?php echo get_field('bottom_link')['title'];?></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="attach-bottom">
		<div class="container">
			<div class="row no-pad-top no-pad-bottom align-items-center justify-content-center">
				<div class="col">
					<a href="#primary">
						<p class="center">Learn More<br/>
						<i class="fas fa-2x fa-chevron-circle-down"></i></p>
					</a>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<section class="white-bg">
			<div class="container">
				<div class="row justify-content-center align-items-center no-pad-bottom">
					<?php
                    // Get first row data from WordPress Post
                    $row = get_field('first_row');
                    ?>
					<div class="col-md-10">
						<h3 class="black center"><?php echo $row['centered_headline'];?></h3>
					</div>
				</div>
			</div>
			<div class="container">
				<div class="row align-items-center">
					<?php
                    // Get second row data from WordPress Post
                    $row = get_field('second_row');
                    ?>
					<div class="col-sm-12 col-md-5 col-lg-5">
						<h2 class="section-title black"><?php echo $row['row_headline'];?></h2>
						<?php echo $row['row_content'];?>
						<div class="underline-link-wrapper">
					      	<a href="<?php echo esc_url($row['row_link']['url']);?>" class="underline underline-lro" title="<?php echo esc_attr($row['row_link']['title']);?>"><?php echo $row['row_link']['title'];?><i class="fas fa-chevron-right"></i></a>
					    </div>
						
					</div>
					<div class="col-sm-12 col-md-6 offset-md-1">
						<div class="image_wrapper center">
							<img src="<?php echo esc_url($row['row_image']['url']);?>" alt="<?php echo esc_attr($row['row_image']['alt']);?>" />
						</div>
					</div>
				</div>
				<div class="row align-items-center">
					<?php
                    // Get third row data from WordPress Post
                    $row = get_field('third_row');
                    ?>
					<div class="col-sm-12 col-md-5 col-lg-5">
						<h2 class="section-title black"><?php echo $row['row_headline'];?></h2>
						<?php echo $row['row_content'];?>
						<div class="underline-link-wrapper">
						      	<a href="<?php echo esc_url($row['row_link']['url']);?>" class="underline underline-lro" title="<?php echo esc_attr($row['row_link']['title']);?>"><?php echo $row['row_link']['title'];?><i class="fas fa-chevron-right"></i></a>
						    </div>
					</div>
					<div class="col-sm-12 col-md-6 offset-lg-1">
						<div class="image_wrapper center">
							<img src="<?php echo esc_url($row['row_image']['url']);?>" alt="<?php echo esc_attr($row['row_image']['alt']);?>" />
						</div>
					</div>
				</div>
			</div>
		</section>

		<?php get_template_part('page-templates/parts/content-green-row');?>
		
		<section class="white-bg">
			<div class="container">
				<div class="row align-items-center">
					<?php
                    // Get fourth row data from WordPress Post
                    $row = get_field('fourth_row');
                    ?>
					<div class="col-md-6 col-lg-5">
						<h2 class="section-title"><?php echo $row['row_headline'];?></h2>
						<?php echo $row['row_content'];?>
						<div class="underline-link-wrapper">
						      	<a href="<?php echo esc_url($row['row_link']['url']);?>" class="underline underline-lro" title="<?php echo esc_attr($row['row_link']['title']);?>"><?php echo $row['row_link']['title'];?><i class="fas fa-chevron-right"></i></a>
						    </div>
					</div>
					<div class="col-md-6 col-lg-6 offset-lg-1">
						<div class="image_wrapper center">
							<img src="<?php echo esc_url($row['row_image']['url']);?>" alt="<?php echo esc_attr($row['row_image']['alt']);?>" />
						</div>
					</div>
				</div>
				<?php
                /*
                 Get the posts here and display below
                 */
                query_posts(array('order'=>'desc', 'order_by'=>'date', 'posts_per_page'=> '3'));
                if (have_posts()) {
                    ?>
					<div class="blog-posts-wrapper">
						<div class="row no-pad-top no-pad-bottom justify-content-center">
							<div class="col-md-6">
								<h3 class="black center">In Latest News</h3>
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="row no-pad-top">
							<?php
                            while (have_posts()) : the_post(); ?>
								<div class="col-sm-12 col-md-4">
									<div class="blog-item">
					                	<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">	
					                		<div class="blog-item-inner">
					                			<div class="featured-image">
					                				<?php the_post_thumbnail('blog-thumbnail'); ?>
					                			</div>
					                			<div class="blog-info-wrapper">
					                				<h4 class="post-title"><?php the_title(); ?></h4>
					                				<p class="post-excerpt"><?php echo wp_trim_excerpt(); ?></p>
					                				<a class="inline-link" href="<?php the_permalink()?>" title="<?php the_title(); ?>">Read More</a>
					                			</div>
					                		</div>
										</a>
				                	</div>
				                </div>
			                <?php
                            endwhile; ?>
			            </div>
					</div>
				<?php
                }?>
			</div>
		</section>
		<?php
        get_template_part('page-templates/parts/content-bottom');
        ?>
	</main>
</div>
<?php
get_footer();
?>
