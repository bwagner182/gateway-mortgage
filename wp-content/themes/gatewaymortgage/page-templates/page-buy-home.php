<?php
/**
 * Template Name: Buy a Home
 */

get_header(); ?>
<div class="jumbotron hero__buy-home container-fluid" style="background: linear-gradient(to bottom, rgba(0,0,0,0.6), rgba(0,0,0,0.6)), url(<?php echo esc_url(get_field('background_image'));?>); background-size: cover; background-repeat: no-repeat; background-position-y: 40%;">
	<div class="container">
		<div class="row align-items-center">
			<div class="col-md-7 col-lg-5">
				<h1 class="hero-title"><?php the_field('hero_headline');?></h1>
				<p class="hero-content"><?php the_field('hero_text');?></p>
				<div class="clearfix"></div>
				<div class="row no-pad-top no-pad-bottom">
					<div class="col-md-6">
						<a class="btn btn-green btn-large" href="<?php echo get_field('hero_button')['url'];?>" title="<?php echo get_field('hero_button')['title'];?>" target="<?php echo get_field('hero_button')['target'] ? get_field('hero_button')['target'] : '_self';?>"><?php echo get_field('hero_button')['title'];?></a>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="row no-pad-top no-pad-bottom">
					<div class="col-12">
						<div class="underline-link-wrapper">	
							<a class="hero-bottom-link underline underline-lro2 yellow" href="<?php echo get_field('bottom_link')['url'];?>" title="<?php echo get_field('bottom_link')['title'];?>"><?php echo get_field('bottom_link')['title'];?></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<section class="white-bg">
			<div class="container">
				<div class="row align-items-center">
					<?php
                    // Get first row data from WordPress Post
                    $row = get_field('first_row');
                    ?>
					<div class="col-sm-12 col-md-5">
						<h2 class="section-title black"><?php echo $row['row_headline'];?></h2>
						<?php echo $row['row_content'];?>
						<div class="underline-link-wrapper">
					      	<a href="<?php echo esc_url($row['row_link']['url']);?>" class="underline underline-lro" title="<?php echo esc_attr($row['row_link']['title']);?>"><?php echo $row['row_link']['title'];?><i class="fas fa-chevron-right"></i></a>
					    </div>
					</div>
					<div class="col-sm-12 col-md-6 offset-md-1">
						<div class="image_wrapper center">
							<img src="<?php echo esc_url($row['row_image']['url']);?>" alt="<?php echo esc_attr($row['row_image']['alt']);?>" />
						</div>
					</div>
				</div>
			</div>
		</section>

		<?php get_template_part('page-templates/parts/content-green-row');?>

		<section class="white-bg">
			<div class="container">
				<div class="row align-items-center">
					<?php
                    // Get third row data from WordPress Post
                    $row = get_field('third_row');
                    ?>
                    <div class="col-sm-12 col-md-5">
						<h2 class="section-title black"><?php echo $row['row_headline'];?></h2>
						<?php echo $row['row_content'];?>
						<div class="underline-link-wrapper">
					      	<a href="<?php echo esc_url($row['row_link']['url']);?>" class="underline underline-lro" title="<?php echo esc_attr($row['row_link']['title']);?>"><?php echo $row['row_link']['title'];?><i class="fas fa-chevron-right"></i></a>
					    </div>
					</div>
					<div class="col-sm-12 col-md-6 offset-md-1">
						<div class="image_wrapper center">
							<img src="<?php echo esc_url($row['row_image']['url']);?>" alt="<?php echo esc_attr($row['row_image']['alt']);?>" />
						</div>
					</div>
				</div>
				<div class="row align-items-center">
					<?php
                    // Get fourth row data from WordPress Post
                    $row = get_field('fourth_row');
                    ?>
					<div class="col-sm-12 col-md-6">
						<div class="image_wrapper center">
							<img src="<?php echo esc_url($row['row_image']['url']);?>" alt="<?php echo esc_attr($row['row_image']['alt']);?>" />
						</div>
					</div>
					<div class="col-sm-12 col-md-5 offset-md-1">
						<h2 class="section-title black"><?php echo $row['row_headline'];?></h2>
						<?php echo $row['row_content'];?>
						<div class="btn-wrapper">
					      	<a href="<?php echo esc_url($row['row_link']['url']);?>" class="btn btn-green" title="<?php echo esc_attr($row['row_link']['title']);?>"><?php echo $row['row_link']['title'];?></a>
					    </div>
					</div>
				</div>
			</div>
		</section>

		<?php
        get_template_part('page-templates/parts/content-bottom');
        ?>
	</main>
</div>
<?php
get_footer();
?>
