<?php
/**
 * Template Name: Basic Page Template
 *
 * This is an example of a page template. Rename this file whatever you
 * wish.
 *
 * @package gatewaymortgage
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php while (have_posts()) : the_post(); ?>

				<?php get_template_part('parts/content', 'page'); ?>

			<?php endwhile; // end of the loop.?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
