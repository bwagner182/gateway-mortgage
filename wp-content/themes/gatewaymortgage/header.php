<?php

/**
 * Main header for the theme:
 */
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

	
	<?php wp_head(); ?>
</head>
<body <?php body_class();?>>
	
	<header class="navbar navbar-expand-lg sticky-top">
		<div class="navbar-inner">	
			<?php
            get_template_part('page-templates/parts/logo');
            ?>
			<?php
            get_template_part('page-templates/parts/nav');
            ?>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" onclick="navigationToggle()">
				<span class="navbar-toggler-icon"><i class="fad fa-bars fa-2x"></i></span>
			</button>
		</div>
		<?php get_template_part('page-templates/parts/nav-mobile');?>
	</header>

