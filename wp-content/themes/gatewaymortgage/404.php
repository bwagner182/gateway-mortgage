<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package gatewaymortgage
 */

get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<section class="container-fluid purple-bg fullscreen-section align-items-center justify-content-center">
			<div class="container">
				<div class="row flex-row align-items-center justify-content-center">
					<div class="col-md-10 col-lg-8">
						<h1 class="page-title white center">Error!</h1>
						<h2 class="white center h3">Sorry! The page you are looking for doesn't exist.</h2>
						<p class="white center">Here are some helpful links to get you back on track!</p>
						<p class="btn-wrapper btn-center flex-btn center">
							<a class="btn btn-white-underline" href="/buy-a-home/" title="Buy a home">Buy a home</a>
						</p>
						<p class="btn-wrapper btn-center flex-btn center">
							<a class="btn btn-white-underline" href="/home-refinance/" title="Refinance your home">Refinance</a>
						</p>
						<p class="btn-wrapper btn-center flex-btn center">
							<a class="btn btn-white-underline" href="/" title="Return to homepage">Return to homepage</a>
						</p>
					</div>
				</div>
			</div>
		</section>
	</main>
</div>

<?php get_footer(); ?>
