<?php
/**
 * gatewaymortgage functions and definitions
 *
 * @package gatewaymortgage
 */

if (! defined('ABSPATH')) {
    exit;
}

class TG_Theme
{

    /**
     * Theme Common Name
     * @var string
     */
    public $common_name = 'Gateway Mortgage';

    /**
     * Text Domain for the theme
     * @var string
     */
    public $textdomain = 'gatewaymortgage';

    /**
     * Google Tag Manager Code
     * @var string
     */
    public $gtm_code = null;

    /**
     * Create empty post types array
     * @var array
     */
    public $post_types = array();

    /**
     * Create empty taxonomies array
     * @var array
     */
    public $taxonomies = array();

    /**
     * The single instance of the class.
     *
     * @var TG_Theme
     */
    protected static $_instance = null;

    /**
     * Main TG_Theme Instance
     *
     * @static
     * @see TG()
     * @return TG Theme - Main Instance.
     */
    public static function instance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }


    public function __construct()
    {
        $this->includes();
        add_action('after_setup_theme', array( $this, 'setup' ));
        add_action('after_setup_theme', array( 'TG_Social', 'register_social_meta_box' ));

        add_action('admin_init', array( $this, '_limit_editor_cap' ));
        add_action('customize_register', array( $this, 'prefix_remove_css_section' ), 15);

        $this->gtm = new TG_GTM($this->gtm_code);
    }

    public function setup()
    {
        load_theme_textdomain($this->textdomain, get_template_directory() . '/languages');

        // Add theme support
        add_theme_support('automatic-feed-links');
        add_theme_support('title-tag');
        add_theme_support('html5', array( 'search-form', 'gallery', 'caption', ));
        add_theme_support('post-thumbnails');
        add_action('widgets_init', array( 'TG_Widgets', 'init' ));

        $this->register_post_types();
        $this->register_taxonomies();
        $this->add_image_sizes();
    }

    /**
     * Bring in all the includes
     * @access private
     */
    private function includes()
    {

        //Pull in hooks
        include_once('includes/tg-hooks.php');

        //admin funcitons
        include_once('includes/admin/class-tg-options.php');
        include_once('includes/admin/class-tg-widgets.php');
        include_once('includes/admin/class-tg-post-types.php');
        include_once('includes/admin/class-tg-taxonomies.php');

        // Pull in classes
        include_once('includes/class-tg-scripts.php');
        include_once('includes/class-tg-gtm.php');
        include_once('includes/class-tg-address.php');
        include_once('includes/class-tg-social.php');

        if (! file_exists(get_template_directory() . '/includes/class-wp-bootstrap-navwalker.php')) {
            // file does not exist... return an error.
            return new WP_Error('class-wp-bootstrap-navwalker-missing', __('It appears the class-wp-bootstrap-navwalker.php file may be missing.', 'wp-bootstrap-navwalker'));
        } else {
            // file exists... require it.
            require_once get_template_directory() . '/includes/class-wp-bootstrap-navwalker.php';
        }

        //Pull in functions
        include_once('includes/tg-core-functions.php');
        include_once('includes/tg-template-tags.php');
        
        //plugin functions
        include_once('includes/tg-aioseo-functions.php');
        include_once('includes/tg-gravity-forms-functions.php');
    }

    /**
     * Limit the Editors Capabilties
     * @access public
     */
    public function _limit_editor_cap()
    {
        $role = get_role('editor');
        $role->add_cap('edit_theme_options');
        $role->remove_cap('switch_themes');
        $role->remove_cap('install_themes');
        $role->remove_cap('edit_themes');
    }

    /**
     * Remove the additional CSS section, introduced in 4.7, from the Customizer.
     * @param $wp_customize WP_Customize_Manager
     */
    private function prefix_remove_css_section($wp_customize)
    {
        $wp_customize->remove_section('custom_css');
    }

    /**
     * Set the Post Types for the theme
     * @access public
     */
    public function register_post_types()
    {
        $this->post_types['video'] = array(
            'labels' => array(
                'name' => 'Videos',
                'singular' => 'Video',
                'plural' => 'Videos'
            ),
            'rewrite' => '',
            'args' => array(
                'menu-icon' => 'dashicons-format-video',
                'taxonomies' => array( 'tax_handle' ),
                'supports' => array(
                    'custom-fields',
                    'title',
                    'thumbnail',
                    'page-attributes'
                ),
                'archive' => true,
                'public' => true,
                'show_ui' => true,
                'show_in_menu' => true,
            )
        );
        

        // Allow for other areas to apply filters to teh post types
        $post_types = $this->post_types;
        $this->post_types = apply_filters('tg_register_post_types', $post_types);

        foreach ($this->post_types as $handle => $args) {
            $post_type = new TG_Post_Type($handle, $args);
        }
    }

    /**
     * Set the Taxonomies for the theme
     * @access public
     */
    public function register_taxonomies()
    {

        /*$this->taxonomies['tax_handle'] = array(
             'post_type' => 'post_type_handle',
             'labels' => array(
                 'name' => 'foo',
                 'singular_name' => 'gatewaymortgage'
             ),
             'args' => array(
                 'hierarchical' => true,
                 'show_in_nav_menus' => false
             )
         );*/

        // Allow for the taxonomies to be filtered
        $taxonomies = $this->taxonomies;
        $this->taxonomies = apply_filters('tg_register_taxonomies', $taxonomies);

        foreach ($this->taxonomies as $handle => $args) {
            $taxonomy = new TG_Taxonomy($handle, $args);
        }
    }

    /**
     * Set Custome Image sizes for the theme
     * @access private
     */
    private function add_image_sizes()
    {
        //add_image_size( $name, $width, $height, $crop );
        add_image_size('blog-thumbnail', 600, 400, true);
        add_image_size('hero', 1200, 800, true);
    }
}

/**
 * Main instance of StreeFreeHealthTesting.
 *
 * @return WooCommerce
 */
function TG()
{
    return TG_Theme::instance();
}

$GLOBALS['tg'] = new TG_Theme();
