<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package edge
 */
get_header();

?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
		
		<div class="page-header-wrapper">
				<div class="container">
					<div class="row flex-row justify-content-center">
						<div class="col-md-10 col-lg-8">
								
                            <h1 class="black center page-title">Gateway Mortgage Blog</h1> 
                            
                            <h3 class="page-subheader center">Everything You Need To Know About Mortgages In One Convenient Place</h3>
						</div><!-- .page-header -->
					</div>
				</div>
			</div>

		<?php get_template_part('page-templates/parts/list', get_post_type());?>	
		
		<?php get_template_part('page-templates/parts/content-bottom');?>

		</main><!-- #main -->
	</div><!-- #primary -->
<?php get_footer(); ?>
